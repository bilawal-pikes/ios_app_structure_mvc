//
//  BaseService.h
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "NetworkManager.h"
#import "DataManager.h"
#import "User.h"
#import "Product.h"
#import "CategoryProduct.h"
#import "Order.h"
#import "Payment.h"
// Categories
#import "UIImage+JS.h"


typedef void (^serviceSuccess)(id data);
typedef void (^serviceFailure)(NSError *error);



@interface BaseService : NSObject

//@property (nonatomic, strong) NSString * currentUserID;

+ (NSString *) currentUserID;

@end
