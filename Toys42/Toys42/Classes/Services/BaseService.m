//
//  BaseService.m
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "BaseService.h"

@implementation BaseService

+ (NSString *) currentUserID{

    return [DataManager sharedManager].currentUser.userID;
}

@end
