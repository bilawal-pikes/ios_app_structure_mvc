//
//  AccountService.m
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "AccountService.h"

@implementation AccountService




+(void)forgetPasswordWhereEmail:(NSString *)email
                        succuss:(serviceSuccess)success
                        failure:(serviceFailure)failure{

    NSString *forget_uri = [NSString stringWithFormat:URI_FORGET_PASSWORD , email];
    
    [NetworkManager getURI:forget_uri parameters:nil success:^(id data) {
        success(data);
        
    } failure:^(NSError *error) {
        failure(error);
        
    }];
    
}

+ (void)getUserStatusWithSuccess:(serviceSuccess)success
                         failure:(serviceFailure)failure{

    NSString * uri_user_status = [NSString stringWithFormat:URI_USER_STATUS,[self currentUserID]];

    
    [NetworkManager getURI:uri_user_status parameters:nil success:^(id data) {
        
        
        BOOL isSuccess = [data[@"success"]  boolValue];
        
        
        if (isSuccess) {
    [[DataManager sharedManager].currentUser setStatus:data[@"user"][@"status"]];

        }else {
            
            [[DataManager sharedManager].currentUser setStatus:data[@"user"][@"status"]];
            
        }
      
        success(@1);
        
        
//        if (data)   {
//            [[DataManager sharedManager].currentUser setStatus:data[@"user"][@"status_key"]];
//
//            
//            success (@1);
//    }
//     else
//     {
//         success(@0);
//     }
//        
        
    } failure:^(NSError *error) {
        failure(error);

        
    }];
    

}


+(void)loginUserWithEmail:(NSString *)email
                 password:(NSString *)pass
               deviceType:(BOOL)deviceType
                  success:(serviceSuccess)success
                  failure:(serviceFailure)failure{

    if (![DataManager sharedManager].deviceToken) {
        [DataManager sharedManager].deviceToken = @"SIMULATOR - No Device Token";
    }

    NSDictionary * userParam = @{
                                 @"user":@{
                                         KEY_EMAIL:email,
                                         KEY_PASSWORD:pass,
                                         KEY_DEVICE_TYPE:@(deviceType).stringValue,
                                         KEY_DEVICE_TOKEN:[DataManager sharedManager].deviceToken

                                         }
                                 };
    
    NSLog(@"userParam: %@",userParam);
    
    [NetworkManager postURI:URI_SIGN_IN
                 parameters:userParam
                    success:^(id data) {
                        
                        
                        if (data) {
                            
                            User * user = [User new];
                            user.name = data[KEY_FULL_NAME];
                            user.email = data[KEY_EMAIL];
                            user.userID = data[KEY_USER_ID];
                            [[DataManager sharedManager] setCurrentUser:user];
                            
                            [[DataManager sharedManager] setAuthToken:data[KEY_AUTH_TOKEN]];
                            success (@1);
                        }
                        else
                        {
                            success(@0);
                        }
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];

}



+ (void) registerUserWithName:(NSString *) name
                        email:(NSString *) email
                     password:(NSString *) password
                  bankAccount:(NSString *) bankAccount
                       client:(NSString *)clientAccount
                   deviceType:(BOOL)deviceType
      corporateIdentification:(NSString *) corporateIdentification
                    tradeName:(NSString *) tradeName
              tributacianCode:(NSString *) tributacianCode
               termsCondition:(BOOL)termsCondition
                     success:(serviceSuccess)success
                     failure:(serviceFailure)failure{


    
    NSDictionary * userParam = @{
                                 @"user":@{
                                         KEY_FULL_NAME:name,
                                         KEY_EMAIL:email,
                                         KEY_PASSWORD:password,
                                         KEY_BANK_ACCOUNT:bankAccount,
                                         KEY_CLIENT_ACCOUNT:clientAccount,
                                         KEY_DEVICE_TYPE:@(deviceType).stringValue,
                                         KEY_CORPORATE_IDENTIFICATION:corporateIdentification,
                                         KEY_TRADE_NAME:tradeName,
                                         KEY_TRIBUTACIAN_CODE:tributacianCode,
                                         KEY_TERMS:@(termsCondition).stringValue,
                                         KEY_DEVICE_TOKEN:[DataManager sharedManager].deviceToken
                                         }
                                 };
    
    NSLog(@"userParam: %@",userParam);

    
    [NetworkManager postURI:URI_SIGN_UP
                 parameters:userParam
                    success:^(id data) {
                        
                        
                        if (data) {
                            
                            User * user = [User new];
                            user.name = data[KEY_FIRST_NAME];
                            user.email = data[KEY_EMAIL];
                            user.userID = data[KEY_USER_ID];

                            [[DataManager sharedManager] setCurrentUser:user];
                            
                            [[DataManager sharedManager] setAuthToken:data[KEY_AUTH_TOKEN]];
                            success (data);
                        }
                        else
                        {
                            success(data);
                        }
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}


+(void)registerUserWithFirstName:(NSString *)fName
                        lastName:(NSString *)lName
                           email:(NSString *)email
                        password:(NSString *)password
                            udid:(NSString *)udid
                           isIos:(BOOL)isIos
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure {

    NSDictionary * userParam = @{
                                 @"user":@{
                                         KEY_FIRST_NAME:fName,
                                         KEY_LAST_NAME:lName,
                                         KEY_EMAIL:email,
                                         KEY_PASSWORD:password,
                                         KEY_UDID:udid,
                                         KEY_IS_IOS:@(isIos).stringValue
                                         
                                         }
                                 };
    
    [NetworkManager postURI:URI_SIGN_UP
                 parameters:userParam
                    success:^(id data) {
                        
                        
                        if (data) {
                            
                            User * user = [User new];
                            user.name = data[KEY_FIRST_NAME];
                            user.email = data[KEY_EMAIL];
                            
                            [[DataManager sharedManager] setCurrentUser:user];
                            
                            [[DataManager sharedManager] setAuthToken:data[KEY_AUTH_TOKEN]];
                            success (@1);
                        }
                        else
                        {
                            success(@0);
                        }
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];




}







+(void)updateUserProfileImage:(UIImage *)image
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure{


    
    NSDictionary * userParam = @{
                                 @"user":@{
                                         @"attachment":[image base64String]
                                         }
                                 };
    
    
    NSString * uri_update_profile = [NSString stringWithFormat:URI_USER_STATUS,[self currentUserID]];
    
    
    [NetworkManager putURI:uri_update_profile parameters:userParam success:^(id data) {
        
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}


+ (void)getUserInfoWithSuccess:(serviceSuccess)success
                       failure:(serviceFailure)failure {


    NSString * uri_update_profile = [NSString stringWithFormat:URI_USER_STATUS,[self currentUserID]];
    
    
    [NetworkManager getURI:uri_update_profile parameters:nil success:^(id data)
     {
         User *user = [[User alloc ] initWithDictionary:data[@"user"]];

         success(user);
     } failure:^(NSError *error) {
         
         failure(error);
     }];
    
    
    
}



























@end
