//
//  AccountService.h
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "BaseService.h"

@interface AccountService : BaseService


+(void) loginUserWithEmail:(NSString *) email
                  password:(NSString *) pass
                deviceType:(BOOL)deviceType
                   success:(serviceSuccess) success
                   failure:(serviceFailure) failure;


+ (void) registerUserWithName:(NSString *) name
                                email:(NSString *) email
                                password:(NSString *) password
                                bankAccount:(NSString *) bankAccount
                            client:(NSString *)clientAccount
                                deviceType:(BOOL)deviceType
                                corporateIdentification:(NSString *) corporateIdentification
                                tradeName:(NSString *) tradeName
                                tributacianCode:(NSString *) tributacianCode
                                termsCondition:(BOOL)termsCondition
                      success:(serviceSuccess) success
                           failure:(serviceFailure) failure;


+ (void) forgetPasswordWhereEmail : (NSString *) email
                           succuss:(serviceSuccess) success
                           failure:(serviceFailure) failure;





+(void) getUserStatusWithSuccess:(serviceSuccess)success
                         failure:(serviceFailure)failure;



+(void)updateUserProfileImage:(UIImage *)image
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure;

+(void)getUserInfoWithSuccess:(serviceSuccess)success
                      failure:(serviceFailure)failure;


@end
