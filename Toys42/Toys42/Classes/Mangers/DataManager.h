//
//  DataManager.h
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Product.h"
@interface DataManager : NSObject



@property (strong, nonatomic) NSString *totalAmount;
@property (strong, nonatomic) NSString *totalDiscount;
@property (assign, nonatomic) DiscountType totalDiscountType;
@property (strong, nonatomic) NSString *tax;

@property (strong, nonatomic) User *currentUser;

@property (strong, nonatomic) NSString *authToken;
@property (strong, nonatomic) NSString *deviceToken;
@property (assign, nonatomic) BOOL isApprovedUser;

@property (strong, nonatomic) NSMutableArray *categories;

+ (DataManager *) sharedManager;

@end
