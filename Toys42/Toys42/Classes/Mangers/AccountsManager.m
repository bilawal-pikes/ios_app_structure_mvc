//
//  ReachabilityManager.m
//  Poco
//
//  Created by Coeus_Dev on 08/04/2013.
//  Copyright (c) 2013 Coeus Solution GmbH. All rights reserved.
//

#import "AccountsManager.h"
#import "UIAlertView+JS.h"

#define FB_APP_ID    @"808426112598026"


#define FB_Account_Not_found    @"Account not found. Please set up your Facebook account in phone settings."
#define TW_Account_Not_found    @"Account not found. Please set up your Twitter account in phone settings."
#define iLocal_Msg_Disabled     @"Please enable iLocal from the phone settings."

@interface AccountsManager ()

@end

@implementation AccountsManager


- (NSString *) validStringForObject:(NSString *) object{
    
    if (object)
        return object;
    
    return @"";
    
}

#pragma mark - Class methods

+ (AccountsManager *)sharedManager
{
    static AccountsManager* sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance=[[AccountsManager alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Instance methods

-(id)init
{
    self=[super init];
    if (self) {

    }
    return self;
}


-(void) showMessageString:(NSString *)message{
    
    [UIAlertView showAlertMessage:message];
}

-(void) verifyData:(NSDictionary *) data{

    if (data[@"error"]) {
        NSDictionary * dict = data[@"error"];
        [self performSelectorOnMainThread:@selector(showMessageString:) withObject:dict[@"message"] waitUntilDone:YES];
    }
}

-(void) getTwitterInfoWithCompletionHandler:(void(^)(User* accDict))completionHandler errorHandler:(void(^)(NSError *error))errorHandler
{

    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) // check Twitter is configured in Settings or not
    {
        NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/users/lookup.json"];
        
        ACAccountStore *accountStore=[[ACAccountStore alloc] init];
        
        ACAccountType *accountType=[accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        __block NSArray *twAccounts=nil;
        
        [accountStore requestAccessToAccountsWithType:accountType options:NULL completion:^(BOOL granted, NSError *error)
         {
             if (granted)
             {
                 twAccounts = [accountStore accountsWithAccountType:accountType];
                 
                 self.twAccount = [twAccounts lastObject];
                 NSLog(@"Twitter UserName: %@, FullName: %@", self.twAccount.username, self.twAccount.userFullName);
                 NSLog(@"Twitter Identifier: %@, Desp: %@", self.twAccount.identifier, self.twAccount.accountDescription);
                 
                 NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys: self.twAccount.username,@"screen_name",nil];

                 SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:url                                      parameters:params];
                 
                 [request setAccount:self.twAccount];

                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    
                                    [NSURLConnection sendAsynchronousRequest:request.preparedURLRequest queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response1, NSData *data, NSError *error)
                                     {
                                         dispatch_async(dispatch_get_main_queue(), ^
                                                        {
                                                            if (data)
                                                            {
                                                                
                                                                NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

                                                                NSLog(@"data:%@",list);
                                                                
                                                                if ([list isKindOfClass:[NSArray class]]) {
                                                                    list = [(NSArray *)list firstObject];
                                                                }
                                                                
                                                                User *user = [User new];
                                                                
                                                                NSString *imgURL = [self validStringForObject:list[@"profile_image_url"]];
                                                                imgURL = [imgURL stringByReplacingOccurrencesOfString:@"_normal" withString:@""];
                                                                NSString *name = [self validStringForObject:list[@"name"]];
                                                                NSArray *nameArr = [name componentsSeparatedByString:@" "];
                                                                NSString *firstName = @"";
                                                                NSString *lastName = @"";
                                                                
                                                                if(nameArr.firstObject)
                                                                    firstName = nameArr.firstObject;
                                                                
                                                                if (nameArr.count>1) {
                                                                    
                                                                    if(nameArr.lastObject)
                                                                        lastName = nameArr.lastObject;

                                                                }
                                                                
//                                                                user.twID           = [self validStringForObject:list[@"screen_name"]];
//                                                                user.name           = [self validStringForObject:list[@"name"]];
//                                                                user.showname       = [self validStringForObject:list[@"name"]];
//                                                                user.firstName      = firstName;
//                                                                user.lastName       = lastName;
//                                                                user.email          = [self validStringForObject:list[@"email"]];
//                                                                user.country        = [self validStringForObject:list[@"location"]];
//                                                                user.imageURL       = imgURL;
//
                                                                completionHandler(user);
                                                                
                                                            }
                                                        });
                                     }];
                                });
             }
             else
             {
                 
                 if (error == nil) {
                     
                     NSLog(iLocal_Msg_Disabled);
                     
                     error = [NSError errorWithDomain:@"" code:2014 userInfo:@{
                                                                               NSLocalizedDescriptionKey:iLocal_Msg_Disabled
                                                                               }];
                 }
                 else
                 {
                     NSLog(@"Error in Login: %@", error);
                 }
                 
                 errorHandler(error);

             }
         }];
    }
    else
    {
        NSLog(@"Not Configured in Settings......"); // show user an alert view that Twitter is not configured in settings.
        
        NSError *error = [NSError errorWithDomain:@"" code:2014 userInfo:@{
                                                                  NSLocalizedDescriptionKey:TW_Account_Not_found
                                                                  }];
        errorHandler(error);
    }
}


-(void) checkFbAccountExistCompletionHandler:(void(^)(NSArray* accounts))completionHandler errorHandler:(void(^)(NSError *error))errorHandler{

//    ACAccountStore *accountStore=[[ACAccountStore alloc] init];
//    ACAccountType *accountType=[accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
//    __block NSArray *facebookAccounts=nil;
//    
//    [accountStore requestAccessToAccountsWithType:accountType
//                                          options:@{ACFacebookAppIdKey : FB_APP_ID,
//                                                    ACFacebookPermissionsKey : @[@"email"],
//                                                    ACFacebookAudienceKey : ACFacebookAudienceFriends}
//                                       completion:^(BOOL granted, NSError *error) {
//                                           
//                                           NSLog(@"%@",error.localizedDescription);
//                                           
//                                           if (granted)
//                                           {
//                                               NSLog(@"%@",facebookAccounts);
//                                           }
//                                           
//                                           else
//                                           {
//                                               dispatch_async(dispatch_get_main_queue(), ^{
//                                                   
//                                                   // Fail gracefully...
//                                                   NSLog(@"%@",error.description);
//                                                   
//                                                   if (error == nil) {
//                                                       NSLog(iLocal_Msg_Disabled);
//                                                       [UIAlertView showAlertMessage:iLocal_Msg_Disabled];
//                                                       
//                                                   }
//                                                   
//                                                   else if([error code]== ACErrorAccountNotFound){
//                                                       
//                                                       [UIAlertView showAlertMessage:FB_Account_Not_found];
//                                                       
//                                                   }
//                                                   else
//                                                   {
//                                                       [UIAlertView showAlertMessage:error.localizedDescription];
//                                                   }
//                                               });
//                                               
//                                               errorHandler(error);
//                                           }
//                                           
//                                       }];
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
     
        ACAccountStore *accountStore=[[ACAccountStore alloc] init];
        
        ACAccountType *accountType=[accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        
        __block NSArray *fbAccounts=nil;
        
        [accountStore requestAccessToAccountsWithType:accountType options:@{ACFacebookAppIdKey : FB_APP_ID,
                                                                            ACFacebookPermissionsKey : @[@"email"],
                                                                            ACFacebookAudienceKey : ACFacebookAudienceFriends} completion:^(BOOL granted, NSError *error)
         {
             if (granted)
             {
                 fbAccounts = [accountStore accountsWithAccountType:accountType];
                 
                 self.fbAccount = [fbAccounts lastObject];
                 NSLog(@"FB UserName: %@, FullName: %@", self.fbAccount.username, self.fbAccount.userFullName);
                 NSLog(@"FB Identifier: %@, Desp: %@", self.fbAccount.identifier, self.fbAccount.accountDescription);
                
                 completionHandler(fbAccounts);
             }
             else
             {
                 if (error == nil) {
                     error = [NSError errorWithDomain:@"" code:2014 userInfo:@{
                                                                               NSLocalizedDescriptionKey:iLocal_Msg_Disabled
                                                                               }];
                 }
                 else
                 {
                     NSLog(@"Error in Login: %@", error);
                 }
                 errorHandler(error);
             }
         }];
    }
    else
    {
        NSLog(@"Not Configured in Settings......");
        
        NSError *error = [NSError errorWithDomain:@"" code:2014 userInfo:@{
                                                                           NSLocalizedDescriptionKey:TW_Account_Not_found
                                                                           }];
        errorHandler(error);
    }
}


-(void) getAccessTokenWithCompletionHandler:(void(^)(NSArray* accounts))completionHandler errorHandler:(void(^)(NSError *error))errorHandler{
    // Initialize the account store
    
//    [self twitter];
//    
//    return;
    
    ACAccountStore *accountStore=[[ACAccountStore alloc] init];
    ACAccountType *accountType=[accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    __block NSArray *facebookAccounts=nil;
    
    [accountStore requestAccessToAccountsWithType:accountType
                                          options:@{ACFacebookAppIdKey : FB_APP_ID,
                                              ACFacebookPermissionsKey : @[@"email"],
                                                 ACFacebookAudienceKey : ACFacebookAudienceFriends}
                                       completion:^(BOOL granted, NSError *error) {
        
        
        
        NSLog(@"%@",error.localizedDescription);
        
                                           if (granted)
                                           {
                                               
                                               
                                               facebookAccounts=[accountStore accountsWithAccountType:accountType];
                                               
                                               _fbAccount = [facebookAccounts lastObject];
                                                                                              
                                               [accountStore renewCredentialsForAccount:_fbAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
                                                   
                                               }];
                                               
                                               NSLog(@"%@",_fbAccount.username);
                                               NSLog(@"%@",_fbAccount.accountDescription);

                                               
                                               [accountStore requestAccessToAccountsWithType:accountType
                                                                                     options:@{ACFacebookAppIdKey : FB_APP_ID,
                                                                                               ACFacebookPermissionsKey : @[@"publish_stream"],
                                                                                               ACFacebookAudienceKey : ACFacebookAudienceFriends}
                                                                                  completion:^(BOOL granted, NSError *error) {
                                                                                      
                                                                                      if (error) {
                                                                                          errorHandler(error);
                                                                                      }
                                                                                      else
                                                                                      {
                                                                                          
                                                                                          [accountStore requestAccessToAccountsWithType:accountType
                                                                                                                                options:@{ACFacebookAppIdKey : FB_APP_ID,
                                                                                                                                          ACFacebookPermissionsKey : @[@"publish_actions"],
                                                                                                                                          ACFacebookAudienceKey : ACFacebookAudienceFriends}
                                                                                                                             completion:^(BOOL granted, NSError *error) {
                                                                                                                                 
                                                                                                                                 
                                                                                                                                 if (error) {
                                                                                                                                     errorHandler(error);
                                                                                                                                 }
                                                                                                                                 else
                                                                                                                                 {
                                                                                                                                     
                                                                                                                                     facebookAccounts=[accountStore accountsWithAccountType:accountType];
                                                                                                                                     
                                                                                                                                     _fbAccount = [facebookAccounts lastObject];
                                                                                                                                     
                                                                                                                                     
                                                                                                                                                                                                                                                                          completionHandler(facebookAccounts);
                                                                                                                                 }
                                                                                                                             }];
                                                                                      }
                                                                                  }];
                                               
                                               NSLog(@"%@",facebookAccounts);
                                           }
                                           
                                           else
                                           {
                                             
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                       
                                                       // Fail gracefully...
                                                   NSLog(@"%@",error.description);
                                                   
                                                   if (error == nil) {
                                                       NSLog(iLocal_Msg_Disabled);
                                                       [UIAlertView showAlertMessage:iLocal_Msg_Disabled];

                                                   }

                                                   else if([error code]== ACErrorAccountNotFound){
                                                   
                                                       [UIAlertView showAlertMessage:FB_Account_Not_found];
                                                   
                                                   }
                                                   else
                                                   {
                                                       [UIAlertView showAlertMessage:error.localizedDescription];
                                                   }
                                               });
                                               
                                               errorHandler(error);
                                           }
     
    }];
    
}

-(void) getUserInfoWithCompletionHandler:(void(^)(User* accDict))completionHandler errorHandler:(void(^)(NSError *error))errorHandler
{
 
    if (_fbAccount)
    {
        NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
        
        SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                requestMethod:SLRequestMethodGET
                                                          URL:requestURL
                                                   parameters:nil];
        request.account = _fbAccount;
                
        [request performRequestWithHandler:^(NSData *data,
                                             NSHTTPURLResponse *response,
                                             NSError *error) {
            if(!error)
            {

                NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSDictionary *errorDictionary = [list valueForKey:@"error"];
                
                if (errorDictionary) {
                    
                    NSDictionary *errorDict = @{ NSLocalizedDescriptionKey : errorDictionary[@"message"],
                                                       NSUnderlyingErrorKey : @"", NSFilePathErrorKey : @"" };
                    
                    NSNumber *errorCode = [errorDictionary valueForKey:@"code"];
                    
                    NSError *anError = [[NSError alloc] initWithDomain:errorDictionary[@"type"]
                                                                  code:errorCode.intValue userInfo:errorDict];
                    
                    if ([errorCode isEqualToNumber:[NSNumber numberWithInt:190]]) {

                        errorHandler(anError);
                        NSLog(@"Renew");
                        return;
                    }
                    if ([errorCode isEqualToNumber:[NSNumber numberWithInt:2500]]) {
                        
                        errorHandler(anError);
                        return;
                    }
                }
                
                
                NSLog(@"%@",list);
                
                User *user = [User new];
                
                user.facebookID     = [self validStringForObject:list[@"id"]];
                user.name           = [self validStringForObject:list[@"name"]];
//                user.showname       = [self validStringForObject:list[@"name"]];
                user.firstName      = [self validStringForObject:list[@"first_name"]];
                user.lastName       = [self validStringForObject:list[@"last_name"]];
                user.email          = [self validStringForObject:list[@"email"]];
                user.country        = [self validStringForObject:list[@"location"][@"name"]];
                user.imageURL       = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?height=100&type=normal&width=100",[self validStringForObject:list[@"id"]]];
                
                completionHandler(user);
            }
            else
            {
                errorHandler(error);
            }

        }];
    }
}


-(void) getFriendsListWithCompletionHandler:(void(^)(NSArray* accounts))completionHandler errorHandler:(void(^)(NSError *error))errorHandler{
    
    
    [[AccountsManager sharedManager] getAccessTokenWithCompletionHandler:^(NSArray *accounts) {
        
        if (accounts.count) {
            _fbAccount = accounts.lastObject;
            if (_fbAccount)
            {
  
                SLRequest *friendsListRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                                   requestMethod:SLRequestMethodGET
                                                                             URL: [[NSURL alloc] initWithString:@"https://graph.facebook.com/me/friends"] parameters:nil];
                friendsListRequest.account = _fbAccount;
                [friendsListRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    // Parse response JSON
                    
                    if (error) {
                        errorHandler(error);
                    }
                    else {
                        NSError *jsonError = nil;
                        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseData
                                                                             options:NSJSONReadingAllowFragments
                                                                               error:&jsonError];
                        //            for (NSDictionary *friend in data[@"data"]) {
                        //                NSLog(@"id: %@, name: %@", friend[@"id"], friend[@"name"]);
                        //            }
                        
                        
                        [self verifyData:data];
                        
                        completionHandler(data[@"data"]);
                    }
                }];
            }
            
        }
    } errorHandler:^(NSError *error) {
        errorHandler(error);
    }];
}

-(void) inviteThelistOfFriends:(NSString *) list{

    
    [[AccountsManager sharedManager] getAccessTokenWithCompletionHandler:^(NSArray *accounts) {
        
        if (accounts.count) {
            _fbAccount = accounts.lastObject;
            if (_fbAccount)
            {

            
                NSMutableDictionary *params = @{@"message": @"Please check this App!",
                                                @"method": @"apprequests",
                                                @"to": list}.mutableCopy;
    
                NSString *urlStr = [NSString stringWithFormat:@"https://graph.facebook.com/%@/apprequests", @""];
                
                NSURL *url = [NSURL URLWithString:urlStr];
                
                SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                        requestMethod:SLRequestMethodPOST
                                                                  URL:url
                                                           parameters:params];
                
                request.account = _fbAccount;
                
                [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    
                    
                    
                }];
                
            }
        }
    } errorHandler:^(NSError *error) {
        
    }];
    
}


-(void) shareOnFacebookWhereImageName:(NSString *) imgName
                              message:(NSString *) message
                            imageData:(NSData *) imgData
                withCompletionHandler:(void(^)(NSString* outputString))completionHandler
                         errorHandler:(void(^)(NSError *error))errorHandler
{
    
    [[AccountsManager sharedManager] getAccessTokenWithCompletionHandler:^(NSArray *accounts) {
        
        if (accounts.count) {
            
            _fbAccount = accounts.lastObject;
            
            if (_fbAccount)
            {
                NSURL *url = [NSURL URLWithString:@"https://graph.facebook.com/me/photos"];

                NSDictionary *parameters = @{@"message": message};
                
                SLRequest *facebookRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                                requestMethod:SLRequestMethodPOST
                                                                          URL:url
                                                                   parameters:parameters];
                
                
                [facebookRequest addMultipartData:imgData
                                         withName:imgName
                                             type:@"multipart/form-data"
                                         filename:imgName];
                
                facebookRequest.account = _fbAccount;
                
                [facebookRequest performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *urlResponse, NSError *error)
                 {
                     
                     
                     if (!error) {
                         
                         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                         
                         [self verifyData:dict];
                         
                         NSLog(@"imgID %@",dict[@"id"]);
                         
                         completionHandler(@"Posted successfully!");
                         
                     }
                     else
                     {
                         errorHandler(error);
                     }
                     // Log the result
                 }];
            }
        }
    } errorHandler:^(NSError *error) {
        errorHandler(error);
    }];
}


-(void) shareOnFacebookWhereVideoName:(NSString *) videoName
                              message:(NSString *) message
                            videoData:(NSData *) vidData
                withCompletionHandler:(void(^)(NSString* outputString))completionHandler
                         errorHandler:(void(^)(NSError *error))errorHandler
{
    
    [[AccountsManager sharedManager] getAccessTokenWithCompletionHandler:^(NSArray *accounts) {
        
        if (accounts.count) {
            
            _fbAccount = accounts.lastObject;
            
            if (_fbAccount)
            {
                
//                NSURL *videoURL = [NSURL URLWithString:@"https://graph.facebook.com/me/photos"];

                NSURL *videoURL = [NSURL URLWithString:@"https://graph.facebook.com/me/videos"];
                
                NSDictionary *parameters = @{@"title": message,
                                             @"description": @"Video shared via GoPato Drive"};
                
                SLRequest *facebookRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                                requestMethod:SLRequestMethodPOST
                                                                          URL:videoURL
                                                                   parameters:parameters];

                [facebookRequest addMultipartData:vidData
                                         withName:@"video.mov"
                                             type:@"video/quicktime"
                                         filename:@"video.mov"];
                
                facebookRequest.account = _fbAccount;
                
                [facebookRequest performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *urlResponse, NSError *error)
                 {
                     
                     if (!error) {
                         
                         NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                         
                         [self verifyData:dict];
                         
                         NSLog(@"imgID %@",dict[@"id"]);

                         if (!dict[@"error"])
                             completionHandler(@"Posted successfully!");
                     }
                     else
                     {
                         errorHandler(error);
                     }
                     
                     
                     NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                     
                     if(error)
                         NSLog(@"Error %@", error.localizedDescription);
                     else
                         NSLog(@"%@", responseString);
                     
                     // Log the result
                 }];
            }
        }
    } errorHandler:^(NSError *error) {
        
        errorHandler(error);
        
    }];
}


-(void) shareOnTwitterWhereImageName:(NSString *)imgName
                             message:(NSString *)message
                           imageData:(NSData *)imgData
               withCompletionHandler:(void (^)(NSString *))completionHandler
                        errorHandler:(void (^)(NSError *))errorHandler{

    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) // check Twitter is configured in Settings or not
    {
        
        ACAccountStore *accountStore=[[ACAccountStore alloc] init];
        
        ACAccountType *accountType=[accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        __block NSArray *twAccounts=nil;
        
        [accountStore requestAccessToAccountsWithType:accountType options:NULL completion:^(BOOL granted, NSError *error)
         {
             if (granted)
             {
                 twAccounts = [accountStore accountsWithAccountType:accountType];
                 
                 self.twAccount = [twAccounts lastObject];
                 
                 if (self.twAccount) {
                     
                     dispatch_async(dispatch_get_main_queue(), ^
                                    {
                                        
                                        NSDictionary *param = @{@"status": message};
                                        
                                        NSURL *requestURL = [NSURL URLWithString:@"https://upload.twitter.com/1/statuses/update_with_media.json"];
                                        
                                        SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                                                    requestMethod:SLRequestMethodPOST
                                                                                              URL:requestURL
                                                                                       parameters:param];
                                        
                                        [postRequest addMultipartData:imgData withName:@"media" type:@"image/png" filename:imgName];
                                        
                                        [postRequest setAccount:self.twAccount];
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^
                                                       {
                                                           
                                                           [NSURLConnection sendAsynchronousRequest:postRequest.preparedURLRequest queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response1, NSData *data, NSError *error)
                                                            {
                                                                
                                                                NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

                                                                NSLog(@"%@",dict);
                                                                
                                                                if (!error)
                                                                    completionHandler(@"Posted successfully!");
                                                                
                                                                
                                                            }];
                                                           
                                                       });
                                    });
                     
                 }
             }
             else
             {
                 
                 if (error == nil) {
                     
                     NSLog(iLocal_Msg_Disabled);
                     
                     error = [NSError errorWithDomain:@"" code:2014 userInfo:@{
                                                                               NSLocalizedDescriptionKey:iLocal_Msg_Disabled
                                                                               }];
                 }
                 else
                 {
                     NSLog(@"Error in Login: %@", error);
                 }
                 
                 errorHandler(error);
             }
         }];
    }
    else
    {
        NSLog(@"Not Configured in Settings......"); // show user an alert view that Twitter is not configured in settings.
        
        NSError *error = [NSError errorWithDomain:@"" code:2014 userInfo:@{
                                                                           NSLocalizedDescriptionKey:TW_Account_Not_found
                                                                           }];
        errorHandler(error);
    }
}


-(void) shareOnTwitterWhereVideoName:(NSString *)videoName
                             message:(NSString *)message
                             vidData:(NSData *)vidData
               withCompletionHandler:(void (^)(NSString *))completionHandler
                        errorHandler:(void (^)(NSError *))errorHandler{
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) // check Twitter is configured in Settings or not
    {
        
        ACAccountStore *accountStore=[[ACAccountStore alloc] init];
        
        ACAccountType *accountType=[accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        __block NSArray *twAccounts=nil;
        
        [accountStore requestAccessToAccountsWithType:accountType options:NULL completion:^(BOOL granted, NSError *error)
         {
             if (granted)
             {
                 twAccounts = [accountStore accountsWithAccountType:accountType];
                 
                 self.twAccount = [twAccounts lastObject];
                 
                 if (self.twAccount) {
                     
                     dispatch_async(dispatch_get_main_queue(), ^
                                    {
                                        
                                        NSDictionary *param = @{@"status": message};
                                        
                                        NSURL *requestURL = [NSURL URLWithString:@"https://upload.twitter.com/1/statuses/update_with_media.json"];
                                        
                                        SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                                                    requestMethod:SLRequestMethodPOST
                                                                                              URL:requestURL
                                                                                       parameters:param];
                                        
                                        [postRequest addMultipartData:vidData
                                                             withName:@"media"
                                                                 type:@"video/quicktime"
                                                             filename:videoName];
                                        
                                        [postRequest setAccount:self.twAccount];
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^
                                                       {
                                                           
                                                           [NSURLConnection sendAsynchronousRequest:postRequest.preparedURLRequest queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response1, NSData *data, NSError *error)
                                                            {

                                                                if (!error)
                                                                    completionHandler(@"Posted successfully!");
                                                                
                                                            }];
                                                           
                                                       });
                                    });
                 }
             }
             else
             {
                 
                 if (error == nil) {
                     
                     NSLog(iLocal_Msg_Disabled);
                     
                     error = [NSError errorWithDomain:@""
                                                 code:2014
                                             userInfo:@{
                                                        NSLocalizedDescriptionKey:iLocal_Msg_Disabled
                                                        }];
                 }
                 else
                 {
                     NSLog(@"Error in Login: %@", error);
                 }
                 
                 errorHandler(error);
             }
         }];
    }
    else
    {
        NSLog(@"Not Configured in Settings......"); // show user an alert view that Twitter is not configured in settings.
        
        NSError *error = [NSError errorWithDomain:@""
                                             code:2014
                                         userInfo:@{
                                                    NSLocalizedDescriptionKey:TW_Account_Not_found
                                                    }];
        errorHandler(error);
    }
}


@end
