//
//  PrinterManager.m
//  PayPato
//
//  Created by Bilawal Liaqat on 14/09/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "PrinterManager.h"

@implementation PrinterManager


static PrinterManager *sharedManager;


+ (PrinterManager *) sharedManager
{
    if(sharedManager == nil)
    {
        
        sharedManager = [[PrinterManager alloc] init];
       
    }
    
    return sharedManager;
}



+ (void)printContent:(id)sender {
    
    
//    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
//    pic.delegate = self;
//    
//    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
//    printInfo.outputType = UIPrintInfoOutputGeneral;
//    printInfo.jobName = self.documentName;
//    pic.printInfo = printInfo;
//    
//    UIMarkupTextPrintFormatter *htmlFormatter = [[UIMarkupTextPrintFormatter alloc]
//                                                 initWithMarkupText:self.htmlString];
//    htmlFormatter.startPage = 0;
//    htmlFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
//    pic.printFormatter = htmlFormatter;
//    pic.showsPageRange = YES;
//    
//    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
//    ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
//        if (!completed && error) {
//            NSLog(@"Printing could not complete because of error: %@", error);
//        }
//    };
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        [pic presentFromBarButtonItem:sender animated:YES completionHandler:completionHandler];
//    } else {
//        [pic presentAnimated:YES completionHandler:completionHandler];
//    }
}


@end
