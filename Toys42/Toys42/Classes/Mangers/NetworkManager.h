//
//  NetworkManager.h
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface NetworkManager : NSObject


typedef void (^loadSuccess)(id data);
typedef void (^loadFailure)(NSError *error);

+(void) postURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;

+(void)  getURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;

+(void)  putURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;


+(void) postURI:(NSString *) uri
   withFilePath:(NSURL *) path
 attachmentName:(NSString *) paramName
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;


+(void)  deleteURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;


@end
