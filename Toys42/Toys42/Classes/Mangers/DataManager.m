//
//  DataManager.m
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager



static DataManager *sharedManager;

+ (DataManager *) sharedManager
{
    if(sharedManager == nil)
    {
        
        sharedManager = [[DataManager alloc] init];
        sharedManager.authToken         = @"";
        sharedManager.totalAmount         = @"0";
        sharedManager.totalDiscount         = @"0";
        sharedManager.tax                  =@"0";
        sharedManager.categories = @[].mutableCopy;
    }
    
    return sharedManager;
}
@end
