//
//  Constants.h
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#ifndef PayPato_Constants_h
#define PayPato_Constants_h




/*  ------------------- BASE URL ------------------- */

#if DEBUG
#define BASE_URL            @"http://52.27.26.217"  // Stagging
#else
#define BASE_URL            @"http://52.27.26.217"  // Production
#endif

/* BASE URL END */



/*  ------------------ LOGIN ------------------ */

#define URI_SIGN_UP             @"/api/v1/sign_up/service_provider"
#define URI_SIGN_IN             @"/api/v1/sessions"
#define URI_FORGET_PASSWORD     @"/api/v1/users/reset_password.json?email=%@"
#define URI_SOCIAL_CONNECT      @""
#define URI_USER_STATUS         @"/api/v1/users/%@.json"


/*  LOGIN END */


/*  ------------------ CUSTOMER ------------------ */


#define URI_CUSTOMER_LIST       @"/api/v1/users/%@/customers.json"
#define URI_CUSTOMER_ADD        @"/api/v1/users/%@/customers.json"
#define URI_CUSTOMER_DETAIL     @"/api/v1/users/%@/customers/%@.json"
#define URI_CUSTOMER_UPDATE     @"/api/v1/users/%@/customers/%@.json"
#define URI_CUSTOMER_DELETE     @"/api/v1/users/%@/customers/%@.json"
#define URI_CUSTOMER_SEARCH     @"/api/v1/users/%@/customers.json?search=%@"
#define URI_CUSTOMER_RECENT     @"/api/v1/users/%@/customers.json?recent=true"


/*  ------------------ SELLER ------------------ */


#define URI_SELLER_LIST       @"/api/v1/users/%@/sellers.json"
#define URI_SELLER_ADD        @"/api/v1/users/%@/sellers.json"
#define URI_SELLER_SEARCH     @"/api/v1/users/%@/sellers.json?search=%@"




/*  SELLER END */

/*  ------------------ INVENTORY ------------------ */

#define URI_PRODUCT_ADD         @"/api/v1/users/%@/products.json"
#define URI_PRODUCT_LIST        @"/api/v1/users/%@/products.json"
#define URI_PRODUCT_DETAIL      @"/api/v1/users/%@/products/%@.json"
#define URI_PRODUCT_UPDATE      @"/api/v1/users/%@/products/%@.json"
#define URI_PRODUCT_DELETE      @"/api/v1/users/%@/products/%@.json"

#define URI_PRODUCT_SEARCH              @"/api/v1/users/%@/products.json?search=%@"
#define URI_PRODUCT_SEARCH_CATEGORY      @"/api/v1/users/%@/categories/%@/search_products.json?search=%@"

/*  INVENTORY END */


/*  ------------------ ORDER PRODUCT ------------------ */

#define URI_ORDER_PRODUCT_ADD       @"/api/v1/users/%@/orders/%@/order_products.json"
#define URI_ORDER_PRODUCT_LIST      @"/api/v1/users/%@/orders/%@/order_products.json"
#define URI_ORDER_PRODUCT_DETAIL    @"/api/v1/users/%@/products/%@.json"
#define URI_ORDER_PRODUCT_UPDATE    @"/api/v1/users/%@/products/%@.json"
#define URI_ORDER_PRODUCT_DELETE    @"/api/v1/users/%@/products/%@.json"
#define URI_CATEGORY_LIST           @"/api/v1/users/%@/categories.json"
#define URI_CATEGORY_ADD            @"/api/v1/users/%@/categories.json"
#define URI_VARIANT_ADD                @"/api/v1/users/%@/products/%@/varients.json"


/*  ORDER PRODUCT END */


/*  ------------------ ORDER ------------------ */

#define URI_ORDER_CREATE       @"/api/v1/users/%@/orders.json"


#define URI_ORDER_LIST      @"api/v1/users/%@/orders/%@/order_products.json"

#define URI_ORDER_DETAIL    @"/api/v1/users/%@/orders/%@.json"
#define URI_ORDER_UPDATE    @"/api/v1/users/%@/orders/%@.json"
#define URI_ORDER_DELETE    @""

#define URI_ORDER_REPORT    @"/api/v1/users/%@/reports.json?start_date=\"%@\"&end_date=\"%@\""
#define URI_TAX             @"/api/v1/users/%@/get_tax.json"

/*  ORDER END */


/*  ------------------ ORDER  PAYMENT------------------ */

#define URI_SEND_ORDER_RECEIPT     @"/api/v1/users/%@/orders/%@/send_details.json"
#define URI_PAYMENT_SIGNATURE         @"/api/v1/users/%@/orders/%@/customer_signature.json"
#define URI_PAYMENT_REFUND          @"/api/v1/users/%@/orders/%@/order_payments/%@.json"

/*  ORDER  PAYMENT END */


/*  ------------------ Send Customer API ------------------ */

#define URI_ORDER_PAYMENT_CREATE       @"/api/v1/users/%@/orders/%@/order_payments.json"
#define URI_PAYMENT_LIST       @"/api/v1/users/%@/order_payments.json"
#define URI_PAYMENT_SEARCH      @"/api/v1/users/%@/order_payments.json?name=%@"

/*  ORDER PAYMENT END */







/*  ------------------ API Keys ------------------ */

#define KEY_AUTH_TOKEN              @"auth_token"
#define KEY_USER_ID              @"user_id"
#define KEY_USER_STATUS              @"status_key"
#define KEY_DEVICE_TOKEN            @"device_token"

// SIGN UP AND SIGN IN
#define KEY_FULL_NAME               @"full_name"
#define KEY_FIRST_NAME              @"first_name"
#define KEY_LAST_NAME               @"last_name"
#define KEY_EMAIL                   @"email"
#define KEY_PASSWORD                @"password"
#define KEY_BANK_ACCOUNT            @"bank_account"
#define KEY_DEVICE_TYPE                 @"device_type"
#define KEY_CLIENT_ACCOUNT              @"client_account"
#define KEY_CORPORATE_IDENTIFICATION    @"corporate_identification"
#define KEY_TRADE_NAME                  @"trade_name"
#define KEY_TRIBUTACIAN_CODE            @"tributacian_code"
#define KEY_TERMS                       @"terms_and_conditions"
#define KEY_UDID                        @"udid"
#define KEY_IS_IOS                   @"is_ios"


// PRODUCT
//{"product":{"title":"new", "price":"15","description":"this is new product","category_id": 1,"varients_attributes":[ {"name":"color","option_values_attributes":[{"value":"read"}, {"value":"green"}]}, {"name":"size","option_values_attributes":[{"value":"small"}, {"value":"medium"}]}],"attachment": "BASE64 String"}}

#define KEY_PRODUCT_ID                  @"id"
#define KEY_PRODUCT_TITLE               @"title"
#define KEY_PRODUCT_PRICE               @"price"
#define KEY_PRODUCT_QUNTITY             @"quantity"
#define KEY_PRODUCT_DESCRIPTION         @"description"
#define KEY_PRODUCT_CATEGORY_ID         @"category_id"
#define KEY_PRODUCT_CATEGORY_Name         @"category_name"
#define KEY_PRODUCT_ATTACHMENT         @"attachment"
#define KEY_PRODUCT_VARIANT_ATTRIBUTES @"varients_attributes"
#define KEY_PRODUCT_VARIANT_NAME        @"name"
#define KEY_PRODUCT_OPTION_VALUE_ATTRIBUTES @"option_values_attributes"
#define KEY_PRODUCT_OPTION_VALUE            @"value"




// CUSTOMER
//{"customer":{"name":"test customer","email": "customer1011@paypato.com","phone":"5346546457567567","notes":"this is note","address1":"n.shah", "address2":"lahore","attachment": "BAS364 String"}}
#define KEY_CUSTOMER                 @"customer"

#define KEY_CUSTOMER_ID                 @"id"
#define KEY_CUSTOMER_NAME               @"name"
#define KEY_CUSTOMER_EMAIL              @"email"
#define KEY_CUSTOMER_PHONE              @"phone"
#define KEY_CUSTOMER_NOTE               @"notes"
#define KEY_CUSTOMER_ADDRESS_ONE        @"address1"
#define KEY_CUSTOMER_ADDRESS_TWO        @"address2"
#define KEY_CUSTOMER_STATE              @"state"
#define KEY_CUSTOMER_ATTACHMENT         @"attachment"



// SELLER
//{"seller":{"full_name":"rashid seller","email": "seller@paypato.com","password":"pato12345","device_type":1,,"phone":"5346546457567567","notes":"this is note","address1":"n.shah", "address2":"lahore"}}


#define KEY_SELLER                 @"seller"

#define KEY_SELLER_ID                 @"id"
#define KEY_SELLER_NAME               @"full_name"
#define KEY_SELLER_EMAIL              @"email"
#define KEY_SELLER_PASSWORD            @"password"
#define KEY_SELLER_PHONE              @"phone"
#define KEY_SELLER_NOTE               @"notes"
#define KEY_SELLER_ADDRESS_ONE        @"address1"
#define KEY_SELLER_ADDRESS_TWO        @"address2"
#define KEY_SELLER_STATE              @"state"
#define KEY_SELLER_ATTACHMENT         @"attachment"







// CATEGORY PRODUCTS

 //id: 3 title: "xyz" }
#define KEY_CATEGORY_ID     @"id"
#define KEY_CATEGORY_TITLE @"title"

// ORDER

// {"order":{"total_amount":34,"tax":"abc","customer_id":4, "notes":"abc","is_paid":false,"order_products_attributes":[{"product_id":5, "quantity":12}, {"product_id":6, "quantity":15}],"discount_attributes":{"value":4, "discount_type":1} }}
#define KEY_ORDER           @"order"

#define KEY_ORDER_ID            @"id"
#define KEY_ORDER_TOTAT_AMOUNT  @"total_amount"
#define KEY_ORDER_TAX           @"tax"
#define KEY_ORDER_IS_PAID       @"is_paid"
#define KEY_ORDER_PAYMENT_TYPE  @"payment_type_key"
#define KEY_ORDER_CUSTOMER_ID   @"customer_id"
#define KEY_ORDER_NOTES         @"notes"
#define KEY_ORDER_PRODUCT_ATTRIBUTES @"order_products_attributes"
#define KEY_ORDER_DISCOUNT_ATTRIBUTES   @"discount_attributes"

#define KEY_DISCOUNT_TYPE           @"discount_type"
#define KEY_DISCOUNT_VALUE          @"value"



// Payment

//{"order_payment":{"payment_type":3, "card_name":"Bilawal Ali", "card_no":"5344", "card_type":"bilawal", "expiry_date":"12/15", "card_payment": 23, "cash_payment": 45}}


#define KEY_PAYMENT               @"payment"
#define KEY_PAYMENT_ID               @"id"
#define KEY_PAYMENT_TYPE                @"payment_type"
#define KEY_PAYMENT_FIRST_NAME          @"first_name"
#define KEY_PAYMENT_LAST_NAME           @"last_name"
#define KEY_PAYMENT_CARD_NAME          @"card_name"
#define KEY_PAYMENT_CARD_NUMBER         @"card_no"
#define KEY_PAYMENT_CARD_TYPE           @"card_type"
#define KEY_PAYMENT_CARD_EXPIRY_DATE    @"expiry_date"
#define KEY_PAYMENT_TYPE_KEY            @"payment_type_key"
#define KEY_PAYMENT_CARD                @"card_payment"
#define KEY_PAYMENT_CASH                @"cash_payment"


/* API Keys END */


/*  ----------------- PAY PATO NOTIFICATIONS ------------------ */

#define PPN_KEY_APNS                @"aps"
#define PPN_KEY_ALERT               @"alert"
#define PPN_KEY_BODY                @"body"
#define PPN_KEY_NOTIFICATION_TYPE   @"type"
#define PPN_KEY_DATA               @"data"


#define GPD_KEY_TYPE_User_APPROVAL_STATUS    @"approval status"

#define KEY_USER_STATUS_PENDING                  @"1"
#define KEY_USER_STATUS_APPROVED                 @"2"
#define KEY_USER_STATUS_REJECTED                 @"3"

/* NOTIFICATIONS END */





#define CURRENCY_SYMBOL     @"₡"


#endif
