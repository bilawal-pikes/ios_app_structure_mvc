//
//  PrinterManager.h
//  PayPato
//
//  Created by Bilawal Liaqat on 14/09/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrinterManager : NSObject


+ (PrinterManager *) sharedManager;


@end
