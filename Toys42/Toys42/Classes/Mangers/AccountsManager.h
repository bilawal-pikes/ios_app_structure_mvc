//
//  ReachabilityManager.h
//  Poco
//
//  Created by Coeus_Dev on 08/04/2013.
//  Copyright (c) 2013 Coeus Solution GmbH. All rights reserved.
//

#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "User.h"
#import "SVProgressHUD.h"

@interface AccountsManager : NSObject
{
    
}

//@property (strong, nonatomic) ACAccountStore *accountStore;
@property (strong, nonatomic) ACAccount *fbAccount;
@property (strong, nonatomic) ACAccount *twAccount;

+(AccountsManager *)sharedManager;


// Facebook

-(void) getAccessTokenWithCompletionHandler:(void(^)(NSArray* accounts))completionHandler
                               errorHandler:(void(^)(NSError *error))errorHandler;

-(void) checkFbAccountExistCompletionHandler:(void(^)(NSArray* accounts))completionHandler
                                errorHandler:(void(^)(NSError *error))errorHandler;

-(void) getUserInfoWithCompletionHandler:(void(^)(User* accDict))completionHandler
                            errorHandler:(void(^)(NSError *error))errorHandler;

-(void) getFriendsListWithCompletionHandler:(void(^)(NSArray* accounts))completionHandler
                               errorHandler:(void(^)(NSError *error))errorHandler;

-(void) inviteThelistOfFriends:(NSString *) list;

-(void) shareOnFacebookWhereImageName:(NSString *) imgName
                              message:(NSString *) message
                            imageData:(NSData *) imgData
                withCompletionHandler:(void(^)(NSString* outputString))completionHandler
                         errorHandler:(void(^)(NSError *error))errorHandler;

-(void) shareOnFacebookWhereVideoName:(NSString *) videoName
                              message:(NSString *) message
                            videoData:(NSData *) vidData
                withCompletionHandler:(void(^)(NSString* outputString))completionHandler
                         errorHandler:(void(^)(NSError *error))errorHandler;


// Twitter

-(void) getTwitterInfoWithCompletionHandler:(void(^)(User* accDict))completionHandler
                               errorHandler:(void(^)(NSError *error))errorHandler;

-(void) shareOnTwitterWhereImageName:(NSString *)imgName
                             message:(NSString *)message
                           imageData:(NSData *)imgData
               withCompletionHandler:(void (^)(NSString *))completionHandler
                        errorHandler:(void (^)(NSError *))errorHandler;

-(void) shareOnTwitterWhereVideoName:(NSString *)videoName
                             message:(NSString *)message
                             vidData:(NSData *)vidData
               withCompletionHandler:(void (^)(NSString *))completionHandler
                        errorHandler:(void (^)(NSError *))errorHandler;

@end
