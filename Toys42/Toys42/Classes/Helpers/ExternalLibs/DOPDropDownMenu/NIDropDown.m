//
//  NIDropDown.m
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import "NIDropDown.h"
#import "DataManager.h"
#import "QuartzCore/QuartzCore.h"

#import "CategoryProduct.h"
//#include "InventoryService.h"

@interface NIDropDown ()<UITextFieldDelegate>
{
    
    UITextField * tfCategroy;
}
@property(nonatomic, strong) UITableView *table;
@property (nonatomic, assign) CGPoint postion;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSMutableArray *list;
@property (nonatomic , assign) ViewType viewType;
@end

@implementation NIDropDown

- (id)showDropDownButton:(UIButton *) button andDirection:(NSString *)direction forType:(ViewType)viewType
{
    _btnSender = button;
    _animationDirection = direction;
    self.table = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect btnFrame = button.frame;
        
        _postion = CGPointMake(0, 0);
        _postion.x = btnFrame.origin.x + button.superview.frame.origin.x + button.superview.superview.frame.origin.x;
        _postion.y =  btnFrame.origin.y + button.superview.frame.origin.y + button.superview.superview.frame.origin.y;
        
        self.viewType = viewType;
        
        if (viewType == ViewTypeCategory) {
            self.list = [DataManager sharedManager].categories;
            
        }else {
            
            
            NSDictionary * currencyDic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CurrencyList" ofType:@"plist"]];
            
            self.list = @[].mutableCopy;
            
            for (NSString * value in [currencyDic allValues]) {
                [self.list addObject:value];
            }
        }
        
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(_postion.x , _postion.y + btnFrame.size.height, btnFrame.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-2.5, -2.5);
        }else if ([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(_postion.x , _postion.y + btnFrame.size.height, btnFrame.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-2.5, 2.5);
        }
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 2.5;
        self.layer.shadowOpacity = 0.3;
        
        [self setBackgroundColor:[UIColor whiteColor]];
        _table = [[UITableView alloc] initWithFrame:CGRectMake(_postion.x, _postion.y + btnFrame.size.height , btnFrame.size.width, 0)];
        
        _table.delegate = self;
        _table.dataSource = self;
        _table.layer.cornerRadius = 5;
        _table.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
        _table.separatorColor = [UIColor whiteColor];
        [_table setBackgroundColor:[UIColor whiteColor]];
        
        float topOffset = 5.0;
        
        int rows;
        if (self.list.count >5) {
            rows =5;
        }else {
            rows = @(self.list.count).intValue;
        }
        
        float height;
        
        if (viewType == ViewTypeCategory) {
            [_table setTableFooterView:[self specialFooterView]];
            height= 50.0 + (rows * 36);
        }
        else {
            height= rows * 36;
            
        }
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(_postion.x, topOffset + _postion.y + btnFrame.size.height, btnFrame.size.width, height);
        } else if([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(_postion.x, topOffset + _postion.y + btnFrame.size.height, btnFrame.size.width, height);
        }
        _table.frame = CGRectMake(_postion.x,_postion.y + btnFrame.size.height, btnFrame.size.width, height);
        [UIView commitAnimations];
        
        [self.superview.superview addSubview:_table];
    
        [button.superview.superview.superview addSubview:self];
        
        [self.superview addSubview:_table];
    }
    
    return self;
}

-(void)hideDropDown:(UIButton *)button {
    
    [tfCategroy resignFirstResponder];
    CGRect btn = button.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([_animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(_postion.x, _postion.y + self.btnSender.frame.size.height, btn.size.width, 0);
    }else if ([_animationDirection isEqualToString:@"down"]) {
        self.frame = CGRectMake(_postion.x, _postion.y + self.btnSender.frame.size.height, btn.size.width, 0);
    }
    _table.frame = CGRectMake(_postion.x, _postion.y + self.btnSender.frame.size.height, btn.size.width, 0);
    [UIView commitAnimations];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 36;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        // cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        
    }
    
    if (self.viewType == ViewTypeCategory) {
        CategoryProduct * category = [_list objectAtIndex:indexPath.row];
        cell.textLabel.text = category.title;
        
    }else {
        
        cell.textLabel.text = [_list objectAtIndex:indexPath.row];
        
    }
    
    
    [cell.textLabel setTextColor:[UIColor grayColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    UIView * v = [[UIView alloc] init];
    v.backgroundColor = [UIColor lightGrayColor];
    cell.selectedBackgroundView = v;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hideDropDown:_btnSender];
    
    UITableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
    [_btnSender setTitle:c.textLabel.text forState:UIControlStateNormal];
    
    
    
    if (self.viewType == ViewTypeCategory) {
        CategoryProduct * category = [_list objectAtIndex:indexPath.row];
        [self.delegate dropDownDelegateMethodWithItemName:category.title atIndexPath:indexPath];
        
    }else {
        [self.delegate dropDownDelegateMethodWithItemName:[_list objectAtIndex:indexPath.row] atIndexPath:indexPath];
        
        
    }
    
    
}


-(UIView *) specialFooterView{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50.0)];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    tfCategroy = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, self.frame.size.width-20, 30)];
    [tfCategroy setPlaceholder:@"Agregar"];
    tfCategroy.borderStyle = UITextBorderStyleRoundedRect;
    [tfCategroy setDelegate:self];
    
    [view addSubview:tfCategroy];
    
    return view;
}


-(void) updateFrame{
    int rows;
    if (self.list.count >5) {
        rows =5;
    }else {
        rows = @(self.list.count).intValue;
    }
    
    CGRect nextFrame = CGRectMake(_postion.x, _postion.y + self.btnSender.frame.size.height, self.frame.size.width, ((rows * 36) + 50));
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self setFrame:nextFrame];
        //[self.table setFrame:self.bounds];
        _table.frame = CGRectMake(_postion.x,_postion.y + self.btnSender.frame.size.height, self.btnSender.frame.size.width, self.bounds.size.height);

    }];
}
-(void)enableRequst {
    
    disableRequset = NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (disableRequset) {
        return NO ;
    }
    
    if(textField.text.length > 0){
        disableRequset = YES;
        
        [self performSelector:@selector(enableRequst) withObject:nil afterDelay:2.0];
        
        
        
        // Save
//        
//        [InventoryService addCategoryWithName:textField.text success:^(id data) {
//            
//            [_list addObject:data];
//            [_table reloadData];
//            [self updateFrame];
//            textField.text = @"";
//            
//        } failure:^(NSError *error) {
//            
//            
//            
//        }];
        
        
        
    }
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    

    
   
  
    NSUInteger newLength = [tfCategroy.text length] + [string length] - range.length;
    
    if([textField isEqual:tfCategroy]){
        return (newLength > 15) ? NO : YES;
        
    }
    
    
    
    return YES;
}


@end
