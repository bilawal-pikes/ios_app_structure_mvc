//
//  NIDropDown.h
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {

    ViewTypeCategory = 0,
    ViewTypeCurrency = 1
} ViewType;


@class NIDropDown;
@protocol NIDropDownDelegate
- (void) dropDownDelegateMethodWithItemName:(NSString *) itemTitle atIndexPath: (NSIndexPath *) indexPath;
@end

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource>
{
    UIImageView *imgView;
    BOOL  disableRequset;

}

@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;


- (void)hideDropDown:(UIButton *)b;

- (id)showDropDownButton:(UIButton *) b andDirection:(NSString *)direction forType:(ViewType)viewType;


@end
