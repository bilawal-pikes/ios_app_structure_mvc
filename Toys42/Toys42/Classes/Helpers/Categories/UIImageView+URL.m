//
//  UIImageView+URL.m
//  iOSApps
//
//  Created by Junaid Muhammad.
//  Copyright (c) 2015 Junaid Muhammad. All rights reserved.
//

#import "UIImageView+URL.h"
#import "UIImageView+WebCache.h"

@implementation UIImageView (URL)

- (void) setImageWithURL:(NSString *) strURL
{
    [self sd_setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:[UIImage imageNamed:@"icon_home"]];
}



- (void) setRatingImageForRating:(CGFloat) rating
{
    
//    3.6
    
    NSString * strRating = @"";
    
    int   newRating = rating * 10;
    int   mainRate = newRating / 10;
    int   subRate  = newRating % 10;
    
    if (subRate > 0 && subRate < 5) {
        strRating = [NSString stringWithFormat:@"%0.1f",(float)mainRate + 0.5];
    }
    else if (subRate >= 6 && subRate <= 9) {
        strRating = [NSString stringWithFormat:@"%0.1f",(float)mainRate + 1.0];
    }
    else{
        strRating = [NSString stringWithFormat:@"%0.1f",(float)mainRate];
    }
    
    NSString * bussinessRating = [NSString stringWithFormat:@"ic_rate_%@",strRating];
    
    NSLog(@"bussinessRating = %@",bussinessRating);
    
    [self setImage:[UIImage imageNamed:bussinessRating]];
}


@end
