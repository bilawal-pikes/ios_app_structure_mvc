//
//  NSDate+JS.h
//  FlowerApp
//
//  Created on 31/03/14.
//  Copyright (c) 2014 3pc GmbH Neue Kommunikation. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TYPE_YYYY_MM_dd_HH_mm_ss     @"yyyy-MM-dd HH:mm:ss"

#define TYPE_DAY_MONTH_DD     @"EEEE, LLLL dd"
#define TYPE_DAY_MONTH_DD_HH_mm     @"EEEE, LLLL dd HH:mm"
#define TYPE_DD_MM_YY_HH_mm    @"dd/MM/YY, HH:mm"


#define TYPE_YYYY_MM_dd     @"yyyy-MM-dd"

#define TYPE_DD_MM_YYYY     @"dd-MM-YYYY"
#define TYPE_DD_MM_YY       @"dd-MM-YY"

#define TYPE_MM_DD_YYYY     @"MM-dd-YYYY"
#define TYPE_MM_DD_YY       @"MM-dd-YY"

#define TYPE_DD_MM          @"dd-MM"
#define TYPE_MM_YYYY        @"MM-YYYY"

#define TYPE_DD             @"dd"
#define TYPE_MM             @"MM"
#define TYPE_YYYY           @"YYYY"


@interface NSDate (JS)

- (NSInteger)numberOfDaysUntilDate:(NSDate *)aDate;
- (NSDate *)dateWithNoTime;
- (NSString *)stringValue;
-(NSString *) stringDateLike:(NSString *) TYPE;

- (NSString *) timeAgoSimple;
- (NSString *) timeAgo;
- (NSString *)timeLeftFromDate:(NSDate * )date;
- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit;
- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit dateFormat:(NSDateFormatterStyle)dFormatter andTimeFormat:(NSDateFormatterStyle)tFormatter;
- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit dateFormatter:(NSDateFormatter *)formatter;
- (NSString *)dateTimeAgo;
- (NSString *)dateTimeUntilNow;

+(NSArray *) lastSevenDaysWithDayMonthDD;
+(NSArray *) lastSevenDays;


@end
