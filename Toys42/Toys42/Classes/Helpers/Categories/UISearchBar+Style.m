//
//  UISearchBar+Style.m
//  PayPato
//
//  Created by Bilawal Liaqat on 23/09/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "UISearchBar+Style.h"
#import "UIColor+JS.h"

@implementation UISearchBar (Style)


- (void)addBorder {


    for (id object in [[[self subviews] objectAtIndex:0] subviews])
            {
                if ([object isKindOfClass:[UITextField class]])
                {
                    UITextField *textFieldObject = (UITextField *)object;
        
                    textFieldObject.textColor = [UIColor lightGrayColor];
                    textFieldObject.layer.borderColor = [[UIColor gpOrangeColor] CGColor];
                    [textFieldObject setBackground:[UIImage imageNamed:@"white"]];

                    textFieldObject.layer.borderWidth = 1.0;
                    textFieldObject.layer.cornerRadius = 5.0;
                    textFieldObject.layer.masksToBounds = YES;
                    textFieldObject.clipsToBounds = YES;
                    [textFieldObject setTintColor:[UIColor gpOrangeColor]];
                    break;
                }
            }
        
            [self setSearchFieldBackgroundImage:[UIImage imageNamed:@"white"]  forState:UIControlStateNormal];
        

}
@end
