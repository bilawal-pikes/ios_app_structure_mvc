//
//  NSDate+JS.m
//  FlowerApp
//
//  Created on 31/03/14.
//  Copyright (c) 2014 3pc GmbH Neue Kommunikation. All rights reserved.
//

#import "NSDate+JS.h"

@implementation NSDate (JS)

#ifndef NSDateTimeAgoLocalizedStrings
#define NSDateTimeAgoLocalizedStrings(key) \
NSLocalizedStringFromTableInBundle(key, @"NSDateTimeAgo", [NSBundle bundleWithPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"NSDateTimeAgo.bundle"]], nil)
#endif

- (NSInteger)numberOfDaysUntilDate:(NSDate *)toDate
{

    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit fromDate:self toDate:toDate options:0];
    
    NSLog(@"Days different >>>> %ld", (long)[components day]);

    return [components day];
}

- (NSDate *)dateWithNoTime
{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                             fromDate:self];
    
    NSDate *dateOnly = [calendar dateFromComponents:components];
    
    [dateOnly dateByAddingTimeInterval:(60.0 * 60.0 * 12.0)];           // Push to Middle of day.
    
    NSLog(@"%@ changed to >>>> %@",self, dateOnly);
    
    return dateOnly;
    
}

- (NSString *)stringValue
{
    return [NSDateFormatter localizedStringFromDate:self
                                          dateStyle:NSDateFormatterMediumStyle
                                          timeStyle:NSDateFormatterNoStyle];
}

-(NSString *) stringDateLike:(NSString *) TYPE
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:TYPE];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormat setTimeZone:gmt];
    return [dateFormat stringFromDate:self];
}

- (NSString *)timeAgoSimple
{
    NSDate *now = [NSDate date];
    double deltaSeconds = fabs([self timeIntervalSinceDate:now]);
    double deltaMinutes = deltaSeconds / 60.0f;
    
    int value;
    
    if(deltaSeconds < 60)
    {
        return [self stringFromFormat:@"%%d%@s" withValue:deltaSeconds];
    }
    else if (deltaMinutes < 60)
    {
        return [self stringFromFormat:@"%%d%@m" withValue:deltaMinutes];
    }
    else if (deltaMinutes < (24 * 60))
    {
        value = (int)floor(deltaMinutes/60);
        return [self stringFromFormat:@"%%d%@h" withValue:value];
    }
    else if (deltaMinutes < (24 * 60 * 7))
    {
        value = (int)floor(deltaMinutes/(60 * 24));
        return [self stringFromFormat:@"%%d%@d" withValue:value];
    }
    else if (deltaMinutes < (24 * 60 * 31))
    {
        value = (int)floor(deltaMinutes/(60 * 24 * 7));
        return [self stringFromFormat:@"%%d%@w" withValue:value];
    }
    else if (deltaMinutes < (24 * 60 * 365.25))
    {
        value = (int)floor(deltaMinutes/(60 * 24 * 30));
        return [self stringFromFormat:@"%%d%@mo" withValue:value];
    }
    
    value = (int)floor(deltaMinutes/(60 * 24 * 365));
    return [self stringFromFormat:@"%%d%@yr" withValue:value];
}

- (NSString *)timeAgo
{
    NSDate *now = [NSDate date];
    double deltaSeconds = fabs([self timeIntervalSinceDate:now]);
    double deltaMinutes = deltaSeconds / 60.0f;
    
    int minutes;
    
    if(deltaSeconds < 30)
    {
        return @"Just now";
    }
    else if(deltaSeconds < 60)
    {
        return [self stringFromFormat:@"%%d %@seconds ago" withValue:deltaSeconds];
    }
    else if(deltaSeconds < 120)
    {
        return @"A minute ago";
    }
    else if (deltaMinutes < 60)
    {
        return [self stringFromFormat:@"%%d %@minutes ago" withValue:deltaMinutes];
    }
    else if (deltaMinutes < 120)
    {
        return @"An hour ago";
    }
    else if (deltaMinutes < (24 * 60))
    {
        minutes = (int)floor(deltaMinutes/60);
        return [self stringFromFormat:@"%%d %@hours ago" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 2))
    {
        return @"Yesterday";
    }
    else if (deltaMinutes < (24 * 60 * 7))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24));
        return [self stringFromFormat:@"%%d %@days ago" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 14))
    {
        return @"Last week";
    }
    else if (deltaMinutes < (24 * 60 * 31))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24 * 7));
        return [self stringFromFormat:@"%%d %@weeks ago" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 61))
    {
        return @"Last month";
    }
    else if (deltaMinutes < (24 * 60 * 365.25))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24 * 30));
        return [self stringFromFormat:@"%%d %@months ago" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 731))
    {
        return @"Last year";
    }
    
    minutes = (int)floor(deltaMinutes/(60 * 24 * 365));
    return [self stringFromFormat:@"%%d %@years ago" withValue:minutes];
}



- (NSString *)timeLeftFromDate:(NSDate * )date
{
    NSDate *now = date;
    double deltaSeconds = fabs([self timeIntervalSinceDate:now]);
    double deltaMinutes = deltaSeconds / 60.0f;
    
    int minutes;
    
    if(deltaSeconds < 30)
    {
        return @"Just few seconds";
    }
    else if(deltaSeconds < 60)
    {
        return [self stringFromFormat:@"%%d %@seconds ago" withValue:deltaSeconds];
    }
    else if(deltaSeconds < 120)
    {
        return @"A minute left";
    }
    else if (deltaMinutes < 60)
    {
        return [self stringFromFormat:@"%%d %@minutes left" withValue:deltaMinutes];
    }
    else if (deltaMinutes < 120)
    {
        return @"An hour left";
    }
    else if (deltaMinutes < (24 * 60))
    {
        minutes = (int)ceil(deltaMinutes/60);
        return [self stringFromFormat:@"%%d %@hours left" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 1.5))
    {
        return @"A day left";
    }
    else if (deltaMinutes < (24 * 60 * 6))
    {
        minutes = (int)ceil(deltaMinutes/(60 * 24));
        return [self stringFromFormat:@"%%d %@days left" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 7))
    {
        return @"1 week left";
    }
    else if (deltaMinutes < (24 * 60 * 13))
    {
        minutes = (int)ceil(deltaMinutes/(60 * 24));
        return [self stringFromFormat:@"%%d %@days left" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 14))
    {
        return @"2 weeks left";
    }
    else if (deltaMinutes < (24 * 60 * 20))
    {
        minutes = (int)ceil(deltaMinutes/(60 * 24));
        return [self stringFromFormat:@"%%d %@days left" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 21))
    {
        return @"3 weeks left";
    }
    else if (deltaMinutes < (24 * 60 * 27))
    {
        minutes = (int)ceil(deltaMinutes/(60 * 24));
        return [self stringFromFormat:@"%%d %@days left" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 28))
    {
        return @"4 weeks left";
    }
    else if (deltaMinutes < (24 * 60 * 30))
    {
        minutes = (int)ceil(deltaMinutes/(60 * 24));
        if (minutes == 30) {
            return @"1 month left";
        }
        return [self stringFromFormat:@"%%d %@days left" withValue:minutes];
    }
//    else if (deltaMinutes < (24 * 60 * 29))
//    {
//        minutes = (int)ceil(deltaMinutes/(60 * 24 * 7));
//        return [self stringFromFormat:@"%%d %@weeks left" withValue:minutes];
//    }
    else if (deltaMinutes < (24 * 60 * 61))
    {
        return @"1 month left";
    }
    else if (deltaMinutes < (24 * 60 * 365.25))
    {
        minutes = (int)ceil(deltaMinutes/(60 * 24 * 30));
        return [self stringFromFormat:@"%%d %@months left" withValue:minutes];
    }
    else if (deltaMinutes < (24 * 60 * 731))
    {
        return @"More then a year";
    }
    
//    minutes = (int)floor(deltaMinutes/(60 * 24 * 365));
    return @"More then a year";
}

// Similar to timeAgo, but only returns "
- (NSString *)dateTimeAgo
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate * now = [NSDate date];
    NSDateComponents *components = [calendar components:
                                    NSYearCalendarUnit|
                                    NSMonthCalendarUnit|
                                    NSWeekCalendarUnit|
                                    NSDayCalendarUnit|
                                    NSHourCalendarUnit|
                                    NSMinuteCalendarUnit|
                                    NSSecondCalendarUnit
                                               fromDate:self
                                                 toDate:now
                                                options:0];
    
    if (components.year >= 1)
    {
        if (components.year == 1)
        {
            return @"1 year ago";
        }
        return [self stringFromFormat:@"%%d %@years ago" withValue:components.year];
    }
    else if (components.month >= 1)
    {
        if (components.month == 1)
        {
            return @"1 month ago";
        }
        return [self stringFromFormat:@"%%d %@months ago" withValue:components.month];
    }
    else if (components.week >= 1)
    {
        if (components.week == 1)
        {
            return @"1 week ago";
        }
        return [self stringFromFormat:@"%%d %@weeks ago" withValue:components.week];
    }
    else if (components.day >= 1) // up to 6 days ago
    {
        if (components.day == 1)
        {
            return @"1 day ago";
        }
        return [self stringFromFormat:@"%%d %@days ago" withValue:components.day];
    }
    else if (components.hour >= 1) // up to 23 hours ago
    {
        if (components.hour == 1)
        {
            return @"An hour ago";
        }
        return [self stringFromFormat:@"%%d %@hours ago" withValue:components.hour];
    }
    else if (components.minute >= 1) // up to 59 minutes ago
    {
        if (components.minute == 1)
        {
            return @"A minute ago";
        }
        return [self stringFromFormat:@"%%d %@minutes ago" withValue:components.minute];
    }
    else if (components.second < 5)
    {
        return @"Just now";
    }
    
    // between 5 and 59 seconds ago
    return [self stringFromFormat:@"%%d %@seconds ago" withValue:components.second];
}



- (NSString *)dateTimeUntilNow
{
    NSDate * now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:NSHourCalendarUnit
                                               fromDate:self
                                                 toDate:now
                                                options:0];
    
    if (components.hour >= 6) // if more than 6 hours ago, change precision
    {
        NSInteger startDay = [calendar ordinalityOfUnit:NSDayCalendarUnit
                                                 inUnit:NSEraCalendarUnit
                                                forDate:self];
        NSInteger endDay = [calendar ordinalityOfUnit:NSDayCalendarUnit
                                               inUnit:NSEraCalendarUnit
                                              forDate:now];
        
        NSInteger diffDays = endDay - startDay;
        if (diffDays == 0) // today!
        {
            NSDateComponents * startHourComponent = [calendar components:NSHourCalendarUnit fromDate:self];
            NSDateComponents * endHourComponent = [calendar components:NSHourCalendarUnit fromDate:self];
            if (startHourComponent.hour < 12 &&
                endHourComponent.hour > 12)
            {
                return @"This morning";
            }
            else if (startHourComponent.hour >= 12 &&
                     startHourComponent.hour < 18 &&
                     endHourComponent.hour >= 18)
            {
                return @"This afternoon";
            }
            return @"Today";
        }
        else if (diffDays == 1)
        {
            return @"Yesterday";
        }
        else
        {
            NSInteger startWeek = [calendar ordinalityOfUnit:NSWeekCalendarUnit
                                                      inUnit:NSEraCalendarUnit
                                                     forDate:self];
            NSInteger endWeek = [calendar ordinalityOfUnit:NSWeekCalendarUnit
                                                    inUnit:NSEraCalendarUnit
                                                   forDate:now];
            NSInteger diffWeeks = endWeek - startWeek;
            if (diffWeeks == 0)
            {
                return @"This week";
            }
            else if (diffWeeks == 1)
            {
                return @"Last week";
            }
            else
            {
                NSInteger startMonth = [calendar ordinalityOfUnit:NSMonthCalendarUnit
                                                           inUnit:NSEraCalendarUnit
                                                          forDate:self];
                NSInteger endMonth = [calendar ordinalityOfUnit:NSMonthCalendarUnit
                                                         inUnit:NSEraCalendarUnit
                                                        forDate:now];
                NSInteger diffMonths = endMonth - startMonth;
                if (diffMonths == 0)
                {
                    return @"This month";
                }
                else if (diffMonths == 1)
                {
                    return @"Last month";
                }
                else
                {
                    NSInteger startYear = [calendar ordinalityOfUnit:NSYearCalendarUnit
                                                              inUnit:NSEraCalendarUnit
                                                             forDate:self];
                    NSInteger endYear = [calendar ordinalityOfUnit:NSYearCalendarUnit
                                                            inUnit:NSEraCalendarUnit
                                                           forDate:now];
                    NSInteger diffYears = endYear - startYear;
                    if (diffYears == 0)
                    {
                        return @"This year";
                    }
                    else if (diffYears == 1)
                    {
                        return @"Last year";
                    }
                }
            }
        }
    }
    
    // anything else uses "time ago" precision
    return [self dateTimeAgo];
}



- (NSString *) stringFromFormat:(NSString *)format withValue:(NSInteger)value
{
    NSString * localeFormat = [NSString stringWithFormat:format, [self getLocaleFormatUnderscoresWithValue:value]];
    return [NSString stringWithFormat:localeFormat, value];
}

- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit
{
    return [self timeAgoWithLimit:limit dateFormat:NSDateFormatterFullStyle andTimeFormat:NSDateFormatterFullStyle];
}

- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit dateFormat:(NSDateFormatterStyle)dFormatter andTimeFormat:(NSDateFormatterStyle)tFormatter
{
    if (fabs([self timeIntervalSinceDate:[NSDate date]]) <= limit)
        return [self timeAgo];
    
    return [NSDateFormatter localizedStringFromDate:self
                                          dateStyle:dFormatter
                                          timeStyle:tFormatter];
}

- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit dateFormatter:(NSDateFormatter *)formatter
{
    if (fabs([self timeIntervalSinceDate:[NSDate date]]) <= limit)
        return [self timeAgo];
    
    return [formatter stringFromDate:self];
}

// Helper functions


-(NSString *)getLocaleFormatUnderscoresWithValue:(double)value
{
    NSString *localeCode = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    // Russian (ru)
    if([localeCode isEqual:@"ru"]) {
        int XY = (int)floor(value) % 100;
        int Y = (int)floor(value) % 10;
        
        if(Y == 0 || Y > 4 || (XY > 10 && XY < 15)) return @"";
        if(Y > 1 && Y < 5 && (XY < 10 || XY > 20)) return @"_";
        if(Y == 1 && XY != 11) return @"__";
    }
    
    // Add more languages here, which are have specific translation rules...
    
    return @"";
}


-(NSDate *) previousDay{

    NSDate *now = self;
    
    int daysToAdd = -1;
    
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:daysToAdd];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate *yesterday = [gregorian dateByAddingComponents:components toDate:now options:0];
    NSLog(@"Yesterday: %@", yesterday);

    return yesterday;
}


+(NSArray *) lastSevenDaysWithDayMonthDD{

    NSMutableArray * listOfDays = @[].mutableCopy;
    
    NSDate * date = [NSDate date];
    
    [listOfDays addObject:[date stringDateLike:TYPE_DAY_MONTH_DD]];

    for (int i = 1; i < 7; i++) {
        date = [date previousDay];
        [listOfDays addObject:[date stringDateLike:TYPE_DAY_MONTH_DD]];
    }

    return listOfDays;
}
+(NSArray *) lastSevenDays{
    
    NSMutableArray * listOfDays = @[].mutableCopy;
    
    NSDate * date = [NSDate date];
    
    [listOfDays addObject:date];
    
    for (int i = 1; i < 7; i++) {
        date = [date previousDay];
        [listOfDays addObject:date];
    }
    
    return listOfDays;
}

@end
