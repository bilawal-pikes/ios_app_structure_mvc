//
//  NSString+Additions.m
//  iOSApps
//
//  Created by Bilawal Liaqat.
//

#import "AppSettings.h"
#import "NSString+Additions.h"

@implementation NSString (Additions)

- (NSString*)stringByEscapingNullValues{
    NSString *className = NSStringFromClass([self class]);
    if ([self isKindOfClass:[NSNull class]] ||
        [className isEqualToString:@"(null)"] ||
        [className isEqualToString:@"<null>"] ||
        [className isEqualToString:@"__NSArrayM"] ||
        self.length==0)
    {
        return @"";
    }else{
        return self;
    }
}

- (NSString*)stringByEscapingMultipleSpaces{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSString *trimmedString = [regex stringByReplacingMatchesInString:self
                                                              options:0
                                                                range:NSMakeRange(0, [self length])
                                                         withTemplate:@" "];
    return trimmedString;
}

- (BOOL) isValidEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isValidAddress {
//^[a-zA-Z]+(\s[a-zA-Z]+)?$
 //   NSString *addressRegex = @"[a-zA-Z0-9]+";
    NSString *addressRegex = @"^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$";

    NSPredicate *addressTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", addressRegex];
    return [addressTest evaluateWithObject:self];

}

- (BOOL)isValidAlphabet {

    NSString *addressRegex = @"^[A-Za-z _]*[A-Za-z][A-Za-z _]*$";
    
    NSPredicate *addressTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", addressRegex];
    return [addressTest evaluateWithObject:self];

}

- (BOOL)isValidDigit {

    NSString *addressRegex = @"[0-9]{0,10}";
        NSPredicate *addressTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", addressRegex];
    BOOL isValid = [addressTest evaluateWithObject:self];


    return isValid;
}

- (NSUInteger)countWords
{
    NSUInteger words = 0;
    
    NSScanner *scanner = [NSScanner scannerWithString:self];
    
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    while ([scanner scanUpToCharactersFromSet:whiteSpace  intoString:nil])
        words++;
    
    return words;
}

- (NSString *)toCurrency
{
   /*
    
    NSArray * ids = [NSLocale availableLocaleIdentifiers];
    
    for (NSString * localeIdentifier in ids) {
        
        NSLocale * locale = [NSLocale localeWithLocaleIdentifier:localeIdentifier];
        
        NSString * symbol2 = [locale objectForKey:NSLocaleCurrencyCode];
        
        NSLog(@"%@, %@", localeIdentifier, symbol2);
        
    }
    
    */
    
//    NSLog(@"%@",[NSLocale availableLocaleIdentifiers]);

    NSNumberFormatter *currencyFormatter  = [[NSNumberFormatter alloc] init];
   // [currencyFormatter setGeneratesDecimalNumbers:NO];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setMaximumFractionDigits:2];

    [currencyFormatter setLocale:[NSLocale localeWithLocaleIdentifier:[AppSettings loadAppSettings].selectCurrency]];
    
//    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    [currencyFormatter setRoundingMode: NSNumberFormatterRoundHalfUp];
//    [currencyFormatter setMaximumFractionDigits:2];
//    
    NSString * _value = self;
    
    if (![_value isKindOfClass:[NSString class]]) {
        _value = [NSString stringWithFormat:@"%0.0f",_value.floatValue];
    }
    
    
    if ([self length] == 0)
        _value = @"0";
    
    NSDecimalNumber *value  = [NSDecimalNumber decimalNumberWithString:_value];
    
    
    NSString *output=  [currencyFormatter stringFromNumber:value];

   // NSString *output=  [currencyFormatter stringFromNumber:[NSNumber numberWithFloat:_value.floatValue]];

    return output;
}



//sending nsDecimalNumber _strong to parameter of incompatible type float

//  NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:currency]];
//    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
//    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
//    NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:currency]];





//- (NSString *)currencyToNumber {
//
//
//    NSNumberFormatter *currencyFormatter  = [[NSNumberFormatter alloc] init];
//
//
//
//
//}

























@end
