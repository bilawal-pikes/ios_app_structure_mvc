//
//  NSString+Additions.h
//  iOSApps
//
//  Created by Bilawal Liaqat.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (NSString *)stringByEscapingNullValues;

- (NSString *)stringByEscapingMultipleSpaces;

- (BOOL) isValidEmail;

- (BOOL) isValidAddress;

- (BOOL) isValidPhone;
- (BOOL) isValidDigit;
- (BOOL) isValidAlphabet;


- (NSUInteger)countWords;

- (NSString *)toCurrency;
- (NSString *)currencyToNumber;


@end