//
//  GPTextField.h
//  iOSApps
//
//  Created by Junaid Muhammad.
//  Copyright (c) 2015 Junaid Muhammad. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UITextField (Style)

- (void) setLeftSpaceSize:(CGFloat) size;
- (void) setRightSpaceSize:(CGFloat) size;
@end
