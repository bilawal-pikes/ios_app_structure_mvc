//
//  UIImageView+URL.h
//  iOSApps
//
//  Created by Junaid Muhammad.
//  Copyright (c) 2015 Junaid Muhammad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (URL)

- (void) setImageWithURL:(NSString *) strURL;


- (void) setRatingImageForRating:(CGFloat) rating;

@end
