//
//  UIColor+JS.h
//  iOSApps
//
//  Created by Junaid Muhammad.
//  Copyright (c) 2015 Junaid Muhammad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


//rgb(190, 185, 176)
#define PatoGray        [UIColor colorWithRed:190.0 / 255.0  green:185.0/255.0 blue:176.0/255.0 alpha:1.0f]
//rgb(166, 121, 47)
#define PatoBrown        [UIColor colorWithRed:166.0 / 255.0  green:121.0/255.0 blue:47.0/255.0 alpha:1.0f]
//rgb(228, 222, 186)
#define PatoCream       [UIColor colorWithRed:228.0 / 255.0  green:222.0/255.0 blue:186.0/255.0 alpha:1.0f]
//rgb(255, 214, 0)
#define PatoYello       [UIColor colorWithRed:255.0 / 255.0  green:214.0/255.0 blue:0.0/255.0 alpha:1.0f]
//rgb(238, 173, 40)
#define PatoOrange       [UIColor colorWithRed:238.0 / 255.0  green:173.0/255.0 blue:40.0/255.0 alpha:1.0f]

#define MenuBackgroundGray  [UIColor colorWithRed:107.0 / 255.0  green:106.0/255.0 blue:102.0/255.0 alpha:1.0f]
//
#define signatureColor  [UIColor colorWithRed:232.0 / 255.0  green:232/255.0 blue:232/255.0 alpha:1.0f]



@interface UIColor (JS)


// PATO VERSION ( DRIVE )

+ (UIColor *) gpDarkBgColor;


// GENERAL VERSION (USER)

+ (UIColor *) gplightGrayBgColor;

+ (UIColor *) gplightGrayInactiveBtnColor;

+ (UIColor *) gpOrangeColor;

+ (UIColor *) gpOrangeColorWithHalfAlpha;

+ (UIColor *) gpOrangeColorWithAlpha:(CGFloat ) alpha;

+ (UIColor *) gpMessageOrangeColor;

+ (UIColor *) gpMapCircleStroke;


@end
