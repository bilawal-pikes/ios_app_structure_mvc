//
//  UISearchBar+Style.h
//  PayPato
//
//  Created by Bilawal Liaqat on 23/09/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (Style)


- (void) addBorder;

@end
