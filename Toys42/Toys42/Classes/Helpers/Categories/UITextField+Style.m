//
//  GPTextField.m
//  iOSApps
//
//  Created by Junaid Muhammad.
//  Copyright (c) 2015 Junaid Muhammad. All rights reserved.
//

#import "UITextField+Style.h"

@implementation UITextField (Style)

- (void) setLeftSpaceSize:(CGFloat) size
{
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size, 34.0)];
    [spaceView setBackgroundColor:[UIColor clearColor]];
    self.leftView = spaceView;
    self.leftViewMode = UITextFieldViewModeAlways;
    
}

- (void) setRightSpaceSize:(CGFloat) size
{
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size, 34.0)];
    [spaceView setBackgroundColor:[UIColor clearColor]];
    self.rightView = spaceView;
    self.rightViewMode = UITextFieldViewModeAlways;
}

@end
