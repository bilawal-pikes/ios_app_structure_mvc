//
//  AppSettings.m
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//



#import "AppSettings.h"

@implementation AppSettings


- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if(self) {
        
        self.authToken              = [decoder decodeObjectForKey:@"authToken"];
        self.userID                 = [decoder decodeObjectForKey:@"userID"];
        self.selectCurrency         = [decoder decodeObjectForKey:@"selectCurrency"];
        self.isCardReaderEnable     = [decoder decodeBoolForKey:@"isCardReaderEnable"];
        self.isCashEnable           = [decoder decodeBoolForKey:@"isCashEnable"];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.authToken        forKey:@"authToken"];
    [encoder encodeObject:self.userID           forKey:@"userID"];
    [encoder encodeObject:self.selectCurrency   forKey:@"selectCurrency"];
    [encoder encodeBool:self.isCardReaderEnable forKey:@"isCardReaderEnable"];
    [encoder encodeBool:self.isCashEnable       forKey:@"isCashEnable"];
}



+(AppSettings *) loadAppSettings{
    
    NSData *archivedObject = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppSettings"];
    
    if (archivedObject)
        return (AppSettings *)[NSKeyedUnarchiver unarchiveObjectWithData: archivedObject];
    else
    {
        //    en_US, USD

        
        AppSettings *appSettings = [AppSettings new];
        
        appSettings.authToken               =   @"";
        appSettings.userID                  =   @"";
        appSettings.isCashEnable            =   YES;
        appSettings.isCardReaderEnable      =   YES;
        appSettings.selectCurrency          =   @"en_CR";
        //appSettings.selectCurrency          =   @"en_US";

        
        
        
        return appSettings;
    }
}

-(void)saveTheAppSetting{
    
    NSData *archivedObject = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[NSUserDefaults standardUserDefaults] setObject:archivedObject forKey:@"AppSettings"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
@end
