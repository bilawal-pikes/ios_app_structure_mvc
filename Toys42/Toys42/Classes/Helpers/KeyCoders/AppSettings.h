//
//  AppSettings.h
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSettings : NSObject<NSCoding>

@property (nonatomic, strong) NSString *authToken;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, assign) BOOL isCardReaderEnable;
@property (nonatomic, assign) BOOL isCashEnable;
@property (nonatomic, strong) NSString *selectCurrency;


+(AppSettings *) loadAppSettings;

-(void)saveTheAppSetting;


@end
