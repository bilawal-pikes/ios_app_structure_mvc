//
//  Payment.m
//  PayPato
//
//  Created by Bilawal Liaqat on 03/09/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "Payment.h"





@implementation Payment


- (id)initWithDictionary:(NSDictionary *)responseData {

    self = [super init];

    if (self) {

        self.paymentId = @([responseData[KEY_PAYMENT][KEY_PAYMENT_ID] integerValue]);
        self.cardType =[self validStringForObject:responseData[KEY_PAYMENT][KEY_PAYMENT_CARD_TYPE] ];
        self.cardNumber = [self validStringForObject:responseData[KEY_PAYMENT][KEY_PAYMENT_CARD_NUMBER]];
        self.cardName = [self validStringForObject:responseData[KEY_PAYMENT][KEY_PAYMENT_CARD_NAME]];
        self.expiryDate =[self validStringForObject:responseData[KEY_PAYMENT][KEY_PAYMENT_CARD_EXPIRY_DATE]];
      
        NSString *paymentTypeString = [self validStringForObject:responseData[KEY_PAYMENT][KEY_PAYMENT_TYPE_KEY]];
        
        if ([paymentTypeString isEqualToString:@"cash"]) {
            self.paymentMethod = CASH;
        }else if ([paymentTypeString isEqualToString:@"credit_card"]){
            self.paymentMethod = CARD;
        }
        else if ([paymentTypeString isEqualToString:@"layaway"]){
            self.paymentMethod = LAYAWAY;
        }else {
            self.paymentMethod = SPLIT;
        }
        
      
        self.totoalAmount =  [self validStringForObject:responseData[KEY_ORDER][KEY_ORDER_TOTAT_AMOUNT]];
        self.cashAmount =    [self validStringForObject:responseData[KEY_PAYMENT][@"cash_payment"]];
        self.cardAmount =    [self validStringForObject:responseData[KEY_PAYMENT][@"card_payment"]];
        self.balanceAmount = [NSString stringWithFormat:@"%f",[self validStringForObject:responseData[KEY_PAYMENT][@"balance"]].floatValue];

            self.order = [[Order alloc] initWithDictionary:responseData];
        self.productList = [Product mapProductsFromArray:responseData [@"products"]];
    
    
    }
    
    return self;

}

+ (NSArray *)mapPaymentFromArray:(NSArray *)arrlist {


    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Payment alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;

}

-(NSString*)cardName:(NSInteger)type{

    
    switch (type) {
        case 0:
            return @"unknow";
            break;
        case 1:
            return @"visa";
            break;
            
        case 2:
            return @"mastercard";
            break;
            
        case 3:
            return @"amex";
            break;
            
        case 4:
            return @"diners";
            break;
        case 5:
            return @"discover";
            break;
            
        case 6:
            return @"jcb";
            break;
  
        default:
            return @"unknow";

            break;
    }
}

@end
