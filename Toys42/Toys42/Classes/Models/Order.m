//
//  Order.m
//  PayPato
//
//  Created by Bilawal Liaqat on 12/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "Order.h"
#import "Product.h"
#import "Payment.h"

@implementation Order

/*
 { success: true order: { id: 3 total_amount: "34.0" tax: "0.0" is_paid: false payment_type_key: null }- customer: { id: 4 name: "test customer" email: "customer456@paypato.com" phone: "5346546457567567" notes: "this is note" attachment_url: "/uploads/customer/attachment/4/1440051829" }- discount: { value: "4.0" discount_type: 1 discount_type_key: "fixed" }- products: [2] 0: { id: 5 title: "ab mm" quantity: 12 price: "0.0" description: "this is new product" attachment: "/uploads/product/attachment/5/1440072389" category_id: 1 category_name: "Shirt" }- 1: { id: 6 title: "ab mm" quantity: 15 price: "0.0" description: "this is new product" attachment: "/uploads/product/attachment/6/1440072497" category_id: 1 category_name: "Shirt" }- }
 
 @property (nonatomic , strong) NSNumber *orderId;
 @property (nonatomic , strong) NSNumber *totalAmount;
 @property (nonatomic , strong) NSNumber *tax;
 @property (assign) BOOL isPaid;
 @property (nonatomic , strong) NSString *paymentTypeKey;
 @property (nonatomic , strong) Customer *customer;
 @property (nonatomic , strong) NSString *discountType;
 @property (nonatomic , strong) NSNumber *discountValue;
 @property (nonatomic , strong)NSMutableArray *productList;

 */

- (id)initWithDictionary:(NSDictionary *)responseData {

    
    self = [super init];
    
    if (self) {
        
        self.orderId = @([responseData[@"order"][KEY_ORDER_ID] integerValue]);
        self.totalAmount = @([responseData[@"order"][KEY_ORDER_TOTAT_AMOUNT] floatValue]);
        self.tax = @([responseData[@"order"][KEY_ORDER_TAX] floatValue]);
        self.isPaid = [responseData[@"order"][KEY_ORDER_IS_PAID]  boolValue];
        //self.paymentTypeKey = [self validStringForObject:responseData[KEY_ORDER_PAYMENT_TYPE]];
        
        //self.discountType = [responseData[@"discount"][KEY_DISCOUNT_TYPE] integerValue];
       // self.discountValue = @([responseData[@"discount"][KEY_DISCOUNT_VALUE] integerValue]);
        
      //  self.customer = [[Customer alloc]  initWithDictionary:responseData[@"customer"] ];
        self.productList = [Product mapProductsFromArray:responseData[@"products"]];
        
    }

    
    
    return self;

}


- (id)initOrderForCustomer:(NSDictionary *)responseData {
    
    self = [super init];
    
    if (self) {
      
        self.orderId = @([responseData[@"order"][KEY_ORDER_ID] integerValue]);
        self.totalAmount = @([responseData[@"order"][KEY_ORDER_TOTAT_AMOUNT] floatValue]);
        self.cardType = [[self validStringForObject:responseData[@"payment"][KEY_PAYMENT_CARD_TYPE]] intValue] ;
        
     //   [responseData[@"payment"][KEY_PAYMENT_CARD_TYPE] integerValue];

    }
    
    
    
    return self;


}


+ (NSArray *)mapOrdersForCustomer:(NSArray *)arrlist {

    NSMutableArray * mappedArr = [NSMutableArray new];
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Order alloc]initOrderForCustomer:dic]];
    }
    
    return mappedArr;

}





@end
