//
//  Product.m
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "Product.h"

@implementation Product

// PRODUCT
//{"product":{"upc_code":"534534-44", "title":"fdshfd", "price":"12","quantity":5, "stock_keeping_unit":"fsdfds", "description":"fsdf ffsdfds"}}

//
//#define KEY_PRODUCT_TITLE               @"title"
//#define KEY_PRODUCT_UPC_CODE            @"upc_code"
//#define KEY_PRODUCT_PRICE               @"price"
//#define KEY_PRODUCT_QUNTITY             @"quantity"
//#define KEY_PRODUCT_STOCK               @"stock_keeping_unit"
//#define KEY_PRODUCT_DESCRIPTION         @"description"





- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        //        self.retailPrice = @([responseData[KEY_DEAL_RETAIL_PRICE] integerValue]);

        self.productID = @([responseData[KEY_PRODUCT_ID] integerValue]);
        
        self.title = [self validStringForObject:responseData[KEY_PRODUCT_TITLE]];
        self.price = [self validStringForObject:responseData[KEY_PRODUCT_PRICE]];
        //self.quantity = @([responseData[KEY_PRODUCT_QUNTITY] integerValue]);
        self.quantity = @(1);
        self.productDescription = [self validStringForObject:responseData[KEY_PRODUCT_DESCRIPTION]];
        self.attachment =  [self validStringForObject:responseData[KEY_PRODUCT_ATTACHMENT]];
        self.category = [self validStringForObject:responseData[KEY_PRODUCT_CATEGORY_Name]];
        self.categoryId =  [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[KEY_PRODUCT_CATEGORY_ID]]];

        //self.variantList = [Variant mapVariatsFromArray:responseData[@"varients"]];
        
        
        self.productTotalAmount = @([responseData[@"products_total"] floatValue]);
        self.totalProducts = @([responseData[@"total_products"] integerValue]);
    }
    
    return self;
}




+ (NSArray *)mapProductsFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Product alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
}

@end
