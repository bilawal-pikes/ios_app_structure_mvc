//
//  Product.h
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "BaseEntity.h"
typedef enum : NSUInteger{
    discountTypeAmount = 0,
    discountTypePercent = 1
    
}DiscountType;


@interface Product : BaseEntity





@property (nonatomic, strong) NSNumber * productID;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * category;
@property (nonatomic, strong) NSString * categoryId;

@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSNumber * quantity;
@property (nonatomic, strong) NSString * productDescription;
@property (nonatomic, strong) NSString * attachment;
@property (nonatomic , strong) NSArray *variantList;
@property(nonatomic, assign) DiscountType discountType;
@property( assign) float discountValue;

// for reproting
@property (nonatomic, strong) NSNumber * productTotalAmount;
@property (nonatomic, strong) NSNumber * totalProducts;



- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapProductsFromArray:(NSArray *) arrlist;


@end
