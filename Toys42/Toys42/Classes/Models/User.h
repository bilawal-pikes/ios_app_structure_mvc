//
//  User.h
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "BaseEntity.h"

@interface User : BaseEntity


// {"user":{"full_name":"Rashid Siddique", "email":"rashid@paypato.com", "password":"agent123", "bank_account": "555-45-55", "device_type":1, "client_account": "565345345", "corporate_identification": "6546453342", "trade_name":"Pikessoft", "tributacian_code":"6575675", "terms_and_conditions":true}}
//



//
@property (nonatomic, strong) NSString * status;

@property (nonatomic, strong) NSString * authToken;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * bankAccount;
@property (nonatomic, strong) NSString * clientAccount;
@property (nonatomic, strong) NSString * corporateIdentification;
@property (nonatomic, strong) NSString * tradeName;
@property (nonatomic, strong) NSString * tributacctionCode;

///

@property (nonatomic, strong) NSString * userID;
@property (nonatomic, strong) NSString * firstName;
@property (nonatomic, strong) NSString * lastName;
@property (nonatomic, strong) NSString * profilePicture;
@property (nonatomic, strong) NSString * phone;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * lastSeen;
@property (nonatomic, assign) BOOL isOnline;
@property (nonatomic, strong) NSString * facebookID;
@property (nonatomic, strong) NSString * imageURL;
@property (nonatomic, strong) NSString * country;
@property (nonatomic, strong) NSString * coverPicture;






-(id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapUserFromArray:(NSArray *) arrlist;




@end
