//
//  CategoryProduct.h
//  PayPato
//
//  Created by Bilawal Liaqat on 21/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "BaseEntity.h"

@interface CategoryProduct : BaseEntity


@property (nonatomic, strong) NSString * catID;
@property (nonatomic, strong) NSString * title;
// below properties for reporting 
@property (nonatomic, strong) NSNumber * categoryPercent;
@property (nonatomic, strong) NSNumber * productsTotal;



- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapCategoryFromArray:(NSArray *) arrlist;

+(NSArray *) mapCategories:(NSArray *) arrlist;



@end
