//
//  BaseEntity.h
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "NSString+Additions.h"

@interface BaseEntity : NSObject


-(NSString *) validStringForObject:(NSString *) object;

-(NSString *) completeImageURL:(NSString *) shortURL;

@end
