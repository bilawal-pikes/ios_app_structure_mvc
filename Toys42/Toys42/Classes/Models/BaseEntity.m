//
//  BaseEntity.m
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "BaseEntity.h"

@implementation BaseEntity



- (NSString *) validStringForObject:(NSString *) object{
    
    if (![object isKindOfClass:[NSNull class]])
        return object;
    
    return @"";
    
}
- (NSNumber *) validNumberForObject:(NSString *) object{
    
    if (![object isKindOfClass:[NSNull class]])
        return object;
    
    return @"";
    
}



-(NSString *)completeImageURL:(NSString *)shortURL{
    
    
    if ([shortURL isKindOfClass:[NSNull class]])
        return @"";
    
    if ([shortURL hasPrefix:@"http"]) {
        return shortURL;
    }
    
    if (shortURL) {
        return [BASE_URL stringByAppendingString:shortURL];
    }
    return @"";
}

@end
