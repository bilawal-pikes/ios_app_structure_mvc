//
//  CategoryProduct.m
//  PayPato
//
//  Created by Bilawal Liaqat on 21/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "CategoryProduct.h"

@implementation CategoryProduct





- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.catID = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[KEY_CATEGORY_ID]]];
        self.title = [self validStringForObject:responseData[KEY_CATEGORY_TITLE]];
        self.categoryPercent = @([responseData[@"category_percent"] integerValue]);
        self.productsTotal = @([responseData[@"products_total"] floatValue]);

    }
    
    return self;
}




+ (NSArray *)mapCategoryFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    CategoryProduct *cpAll = [CategoryProduct new];
    cpAll.catID = @"-1";
    cpAll.title = @"All";
    
    [mappedArr addObject:cpAll];
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[CategoryProduct alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
}

+ (NSArray *)mapCategories:(NSArray *)arrlist {


    NSMutableArray * mappedArr = [NSMutableArray new];
  
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[CategoryProduct alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
}


@end
