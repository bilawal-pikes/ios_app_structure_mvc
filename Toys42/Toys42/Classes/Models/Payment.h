//
//  Payment.h
//  PayPato
//
//  Created by Bilawal Liaqat on 03/09/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "BaseEntity.h"
#import "Product.h"
#import "Order.h"


//typedef enum : NSUInteger {
//    UNKNOWN     = 0,
//    VISA        = 1,
//    MASTERCARD  = 2,
//    AMEX        = 3,
//    DINERS      = 4,
//    DISCOVER    =5,
//    JCB         =6
//}CardType;


typedef enum :NSUInteger{
    CASH = 0,
    CARD = 1,
    LAYAWAY= 2,
    SPLIT =3
    
}PaymentMethod;


@interface Payment : BaseEntity

//ayment: { id: 1 card_type: "card type" card_no: "5344" expiry_date: null card_name: " " payment_type_key: "cash" }-


@property (nonatomic ,strong) NSNumber *paymentId;
@property (nonatomic , strong) NSString *cardType;
@property(nonatomic , strong) NSString *cardNumber;
@property(nonatomic , strong) NSString *cardName;
@property (nonatomic , strong) NSString *expiryDate;
//@property (nonatomic , strong) NSString *paymentTypeKey;

@property (assign) PaymentMethod paymentMethod;

@property (nonatomic , strong) NSString *totoalAmount;
@property (nonatomic , strong) NSString *cashAmount;
@property (nonatomic , strong) NSString *cardAmount;
@property (nonatomic , strong) NSString *balanceAmount;


@property (nonatomic , strong) NSArray *productList;
@property (nonatomic ,strong) Order *order;



//@property (nonatomic , assign) CardType *card;


- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapPaymentFromArray:(NSArray *) arrlist;






@end
