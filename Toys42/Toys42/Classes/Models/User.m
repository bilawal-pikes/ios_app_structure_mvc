//
//  User.m
//  PayPato
//
//  Created by Bilawal Liaqat on 04/08/2015.
//  Copyright (c) 2015 PikesSoft. All rights reserved.
//

#import "User.h"

@implementation User



- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        //        self.title = [self validStringForObject:responseData[KEY_PRODUCT_TITLE]];
        
        
        self.userID  = [self validStringForObject:responseData [KEY_USER_ID]];
        self.name  = [self validStringForObject:responseData [KEY_FULL_NAME]];
        self.status  = [self validStringForObject:responseData [KEY_USER_STATUS]];
        self.email  = [self validStringForObject:responseData [KEY_EMAIL]];
        self.profilePicture = [self validStringForObject:responseData [@"attachment"]];
    }
    
    return self;
}




+ (NSArray *)mapUserFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[User alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
}

@end
