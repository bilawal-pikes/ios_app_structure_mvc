//
//  AppDelegate.h
//  Toys42
//
//  Created by Bilawal Liaqat on 22/12/2015.
//  Copyright © 2015 PikesSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

